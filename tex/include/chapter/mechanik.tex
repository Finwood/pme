\chapter{Mechanik}

Im Folgenden sollen zunächst die mechanischen Randbedingungen sowie die
Belastungsverläufe der Walze betrachtet werden, bevor mit dem
Festigkeitsnachweis fortgefahren wird.

\begin{figure}[h]
    \centering
    \footnotesize
    \sffamily
    \svginclude{skizze-walze}
    \caption[Skizze der Walze, nicht maßstabsgetreu.]%
            {Skizze der Walze, nicht maßstabsgetreu.
             Der Wellenabsatz \textsf{\textbf{A}} ist der Sitz für das
             Wälzlager, Absatz \textsf{\textbf{B}} dient als Kontaktfläche für
             einen Radialwellendichtring.}
    \label{fig:skizze-walze}
\end{figure}

\begin{pycode}
MW = format(M_W / 1e3, '.2f')
FW = format(F_W / 1e3, '.0f')
n =  format(n_ein, '.0f')
h1 = format(h_1 / 1e-3, '.0f')
bW = format(b_W / 1e-3, '.0f')
vW = format(v_W / 1e-3, '.0f')
dW = format(d_W / 1e-3, '.0f')
Lh = format(L_h, '.0f')
du = format(d_u / 1e-2, '.0f')
\end{pycode}
\begin{table}
    \centering
    \caption{Konstruktive Randbedingungen, vorgegeben in der Aufgabenstellung}
    \label{tab:vorgabewerte}
    \def\arraystretch{1.3} % increase row height
    \begin{tabular}{lcrl}
        \toprule
        Beschreibung & Zeichen & Wert & Einheit \\
        \midrule
        Walzenmoment & $M_W$ & $\py{MW}$ & \si{\kilo\newton\metre} \\
        Walzenkraft & $F_{W,r}$ & $\py{FW}$ & \si{\kilo\newton} \\
        Eingangsdrehzahl & $n_\text{ein}$ & $\py{n}$ & \si{\per\minute} \\
        Nennhöhe Walzgut (nach Walzvorgang) & $h_1$ & $\py{h1}$ & \si{\milli\metre} \\
        Breite Walzgut & $b_W$ & $\py{bW}$ & \si{\milli\metre} \\
        Geschwindigkeit Walzgut & $v_W$ & $\py{vW}$ & \si{\milli\metre\per\second} \\
        Walzendurchmesser & $d_W$ & $\py{dW}$ & \si{\milli\metre} \\
        Lagerlebensdauer & $L_h$ & $\py{Lh}$ & \si{\hour} \\
        zulässiger Übersetzungsfehler & $\Delta u$ & $\py{du}$ & \si{\percent} \\
        \bottomrule
    \end{tabular}
\end{table}

\section{Kräfteverhältnisse an der Walze}
Die von der Walze auf das Walzgut aufgebrachte Kraft setzt sich aus einer
Vertikal- und einer Horizontalkomponente zusammen, dargestellt in
\fig{freischnitt-walze-yz}. Da für die die Auslegung jedoch nur die reine
Querkraft, das daraus resultierende Biegemoment sowie das Torsionsmoment
relevant sind, ist es sinnvoll die Belastungen dahin zu transformieren. Durch
ein Drehen des Koordinatensystems lassen sich alle Kräfte auf eine Achse legen,
wodurch sich die Berechnung vereinfacht. Das neu definierte Hauptachsensystem
ist in \fig{freischnitt-walze-gedreht} dargestellt.
%
\begin{figure}
    \centering
    \footnotesize
    \begin{subfigure}[t]{0.3\textwidth}
        \centering
        \svginclude{freischnitt-walze-yz}
        \subcaption{Basissystem}
        \label{fig:freischnitt-walze-yz}
    \end{subfigure}
    ~ % add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
    \begin{subfigure}[t]{0.3\textwidth}
        \centering
        \svginclude{freischnitt-walze-gedreht}
        \subcaption{Hauptachsensystem}
        \label{fig:freischnitt-walze-gedreht}
    \end{subfigure}
    \caption[An der unteren Walze angreifende Kräfte.]%
            {An der unteren Walze angreifende Kräfte.
             Für die Berechnung der Schnittgrößen ist es sinnvoll, die Kräfte in
             eine Radialkraft und ein Torsionsmoment zu transformieren.
             Zusätzlich wird ein gedrehtes Koordinatensystem $xy'z'$ eingeführt,
             damit die resultierende Radialkraft entlang einer Achse wirkt.}
    \label{fig:freischnitt-walze}
\end{figure}

Die Massenkräfte der Walze sind relativ zur Gesamtbelastung vernachlässigbar
klein, wie die folgende Überschlagsrechnung zeigt. Hierzu werden die in
\tab{massenkräfte-walze} aufgeführten Konstanten herangezogen.
\begin{pycode}
dichte_stahl = 7.9e3  # kg/m**3, TBM S. 117 / Stahl legiert
erdbeschleunigung = 9.81  # m/s**2, TBM S. 36

volumen_welle = np.pi / 4 * (18**2 * 100 + 100**2 * 100 + 120**2 * 50 + 350**2 * 450) * 1e-9
masse_welle = volumen_welle * dichte_stahl
gewichtskraft_welle = masse_welle * erdbeschleunigung
Fg_rel = gewichtskraft_welle / F_W
\end{pycode}
\begin{table}
    \centering
    \caption{Für die Berechnung der Massenkräfte verwendete Konstanten.}
    \label{tab:massenkräfte-walze}
    \def\arraystretch{1.3} % increase row height
    \begin{tabular}{lcrl}
        \toprule
        Beschreibung & Zeichen & Wert & Quelle \\
        \midrule
        Dichte von Stahl & $\rho_\text{Stahl}$
            & \py{SI(format(dichte_stahl, '.1e'), r'\kilogram\per\cubic\metre')}
            & \cite[S.~117]{tbm} \\
        Erdbeschleunigung & $g$
            & \py{SI(format(erdbeschleunigung, '.2f'), r'\metre\per\square\second')}
            & \cite[S.~36]{tbm} \\
        \bottomrule
    \end{tabular}
\end{table}
\begin{align}
    \begin{split}
        V_\text{Welle} &= \sum_i \frac{\pi d_i^2}{4} \cdot l_i \\
                       &= \frac{\pi}{4} \left( 80^2 \cdot 100 + 100^2 \cdot 100 + 120^2 \cdot 50
                                             + 350^2 \cdot 450 \right) ~ \si{\cubic\milli\metre} \\
                       &= \py{SI(format(volumen_welle, '.3f'), r'\cubic\metre')}
    \end{split} \\
    m_\text{Welle}     &= V_\text{Welle} \cdot \rho_\text{Stahl}
                        = \py{SI(round(masse_welle), r'\kilogram')} \\
    F_{g,\text{Welle}} &= m_\text{Welle} \cdot g
                        = \py{SI(format(gewichtskraft_welle / 1e3, '.1f'), r'\kilo\newton')} \\
    \frac{F_{g,\text{Welle}}}{F_W} &= \py{SI(format(Fg_rel * 100, '.2f'), r'\percent')}
\end{align}

\section{Durchbiegung}
Nach \cite[F.~102]{me2} lässt sich eine Überschlagsrechnung bezüglich
Durchbiegung in der Mitte und Neigung im Lager durchführen. Es herrscht
Grundbelastungsfall~1 mit der im Lager angreifenden, der halben Walzkraft
entsprechenden Kraft $F_L = \frac{F_W,r}{2}
= \py{SI(int(F_W/2 / 1e3), r'\kilo\newton')}$. Es gilt im vorliegenden
Belastungsfall für die Durchbiegung $f$ und die Neigung $\beta$:
\begin{subequations}
    \label{eq:durchbiegung-allgemein}
    \begin{align}
        f &= \frac{F}{3 E} \cdot \left( \frac{l_1^3}{I_{b1}}
                                      + \frac{l_2^3 - l_1^3}{I_{b2}}
                                      + \frac{l_3^3 - l_2^3}{I_{b3}} \right) \\[1ex]
        \beta &= \frac{F}{2 E} \cdot \left( \frac{l_1^2}{I_{b1}}
                                          + \frac{l_2^2 - l_1^2}{I_{b2}}
                                          + \frac{l_3^2 - l_2^2}{I_{b3}} \right)
    \end{align}
\end{subequations}
Nach \cite[S.~49]{tbm} beträgt das Flächenträgheitsmoment eines kreisförmigen
Querschnitts $I_b = \frac{\pi d^4}{64}$. Somit lassen sich
Gleichungen~\eq{durchbiegung-allgemein} umformulieren zu
\begin{subequations}
    \begin{align}
        f &= \frac{F}{3 E} \frac{64}{\pi} \cdot \left( \frac{l_1^3}{d_1^4}
                                                     + \frac{l_2^3 - l_1^3}{d_2^4}
                                                     + \frac{l_3^3 - l_2^3}{d_3^4} \right) \\[1ex]
        \beta &= \frac{F}{2 E} \frac{64}{\pi} \cdot \left( \frac{l_1^2}{d_1^4}
                                                         + \frac{l_2^2 - l_1^2}{d_2^4}
                                                         + \frac{l_3^2 - l_2^2}{d_3^4} \right)
    \end{align}
\end{subequations}

\begin{pycode}
l_1 = 50e-3/2
l_2 = l_1 + 25e-3
l_3 = l_2 + 450e-3/2

d_1 = 110e-3
d_2 = 130e-3
d_3 = 350e-3

E_stahl = 200e9
\end{pycode}

\begin{table}
    \centering
    \caption{Geometrie der Walze bezogen auf \cite[F.~102]{me2}, gemäß
             \fig{skizze-walze}.}
    \label{tab:geometrie-walze-durchbiegung}
    \def\arraystretch{1.3} % increase row height
    \begin{tabular}{lr}
        \toprule
        Größe & Abmessung \\
        \midrule
        $l_1$ & \py{SI(round(l_1/1e-3), r'\milli\metre')} \\
        $l_2$ & \py{SI(round(l_2/1e-3), r'\milli\metre')} \\
        $l_3$ & \py{SI(round(l_3/1e-3), r'\milli\metre')} \\
        $d_1$ & \py{SI(round(d_1/1e-3), r'\milli\metre')} \\
        $d_2$ & \py{SI(round(d_2/1e-3), r'\milli\metre')} \\
        $d_3$ & \py{SI(round(d_3/1e-3), r'\milli\metre')} \\
        \bottomrule
    \end{tabular}
\end{table}

Der Elastizitätsmodul beträgt für Stähle allgemein
$E \approx \py{SI(round(E_stahl / 1e9), r'\giga\pascal')}$, \cite[S.~46]{tbm}.
Eingesetzt ergeben sich mit den in \tab{geometrie-walze-durchbiegung}
zusammengestellten Werten nun Durchbiegung und Neigung:
\begin{pycode}
durchbiegung = F_W / 2 / (3 * E_stahl) * 64 / np.pi * \
               (l_1**3 / d_1**4 + (l_2**3 - l_1**3) / d_2**4 + (l_3**3 - l_1**2) / d_3**4)
neigung = F_W / 2 / (2 * E_stahl) * 64 / np.pi * \
          (l_1**2 / d_1**4 + (l_2**2 - l_1**2) / d_2**4 + (l_3**2 - l_1**2) / d_3**4)
\end{pycode}
\begin{subequations}
    \begin{align}
        f     &= \py{SI(round(durchbiegung / 1e-6), r'\micro\metre')}
               < \SI{250}{\micro\metre} \\[1ex]
        \beta &= \py{SI(format(neigung, '.2e'), r'\radian')}
               = \py{SI(format(neigung / np.pi * 180, '.3f'), r'\degree')}
               = \py{SI(format(neigung / np.pi * 180 * 60, '.3f'), r'\arcminute')}
               < \SI{4}{\arcminute}
    \end{align}
\end{subequations}

Die maximale Durchbiegung beträgt weniger als \SI{250}{\micro\metre}, sodass die
Forderung nach einer maximalen Blechdickenabweichung von \SI{500}{\micro\metre}
erfüllt ist. Gemäß \cite[S.~400]{hr1} ist für ein einreihiges
Zylinderrollenlager eine Schiefstellung von vier Winkelminuten ohne
Beeinträchtigung der Lebensdauer möglich. Folglich stellt auch die Neigung der
Welle an der Lagerstelle kein Problem dar.

\section{Ersatzbild und Belastungsverläufe}

Es kann angenommen werden, dass die Walzkraft und das Walzmoment in Form einer
Streckenlast bzw.  eines Streckenmoments gleichmäßig über die gesamte Breite der
Walze übertragen werden.  \fig{mech-ersatzbild} zeigt das mechanische Ersatzbild
der Walze, die Verläufe der daraus resultierenden Belastungen sind in
\fig{belastungsverläufe-walze} dargestellt.
\begin{figure}
    \centering
    \footnotesize
    \begin{subfigure}{\textwidth}
        \centering
        \svginclude{mech-ersatzbild-xz}
        \subcaption{$xz'$-Ebene}
        \label{fig:mech-ersatzbild-xz}
    \end{subfigure}
    % http://tex.stackexchange.com/a/120704
    \par\bigskip \par\medskip  % force a bit of vertical whitespace
    \begin{subfigure}{\textwidth}
        \centering
        \svginclude{mech-ersatzbild-xy}
        \subcaption{$xy'$-Ebene}
        \label{fig:mech-ersatzbild-xy}
    \end{subfigure}
    %
    \caption[Mechanisches Ersatzbild der unteren Walze.]%
            {Mechanisches Ersatzbild der unteren Walze.
             Der Freischnitt erfolgt in zwei Ebenen des Hauptachsensystems, vgl.
             \fig{freischnitt-walze-gedreht}. In der $xy'$-Ebene fallen alle
             Kräfte weg.}
    \label{fig:mech-ersatzbild}
\end{figure}

\begin{figure}
    \centering
    \footnotesize
    \begin{subfigure}{\textwidth}
        \centering
        \svginclude{walze-verlauf-Qz}
        \subcaption{Querkraft $Q_{z'}$}
        \label{fig:walze-verlauf-Qz}
    \end{subfigure}
    % http://tex.stackexchange.com/a/120704
    \par\bigskip  % force a bit of vertical whitespace
    \begin{subfigure}{\textwidth}
        \centering
        \svginclude{walze-verlauf-My}
        \subcaption{Biegemoment $M_{y'}$}
        \label{fig:walze-verlauf-My}
    \end{subfigure}
    % http://tex.stackexchange.com/a/120704
    \par\bigskip  % force a bit of vertical whitespace
    \begin{subfigure}{\textwidth}
        \centering
        \svginclude{walze-verlauf-Mx}
        \subcaption{Torsionsmoment $M_x$}
        \label{fig:walze-verlauf-Mx}
    \end{subfigure}
    %
    \caption[Belastungsverläufe der unteren Walze.]%
            {Belastungsverläufe der unteren Walze.
             Durch die Wahl des Hauptachsensystems (siehe
             \fig{freischnitt-walze-gedreht}) entfallen die Querkraft in
             $y'$-Richtung sowie das Biegemoment um die $z'$-Achse. Daher sind
             deren Verläufe hier nicht dargestellt.}
    \label{fig:belastungsverläufe-walze}
\end{figure}
