#!/usr/bin/env python3
# coding: utf-8

import sys
import os.path
import re

SVGBUILDDIR = 'img/svg/build'
SVGFILES = []

BIBLATEX_BACKEND = ''
BIBFILES = []

INCLUDES = []

PYTHONTEX = False

preg_svg = re.compile(r'^[^%\r\n]*?\\svginclude{(.+?)}', re.MULTILINE | re.DOTALL)
preg_biblatex_backend = re.compile(r'^[^%\r\n]*?\\usepackage\[.*?backend\s*?=\s*?(\w+).*?\]{biblatex}', re.MULTILINE | re.DOTALL)
preg_bibfiles = re.compile(r'^[^%\r\n]*?\\(addbibresource|bibliography){(.+?)}', re.MULTILINE | re.DOTALL)
preg_includes = re.compile(r'^[^%\r\n]*?\\(input|include){(.+?)}', re.MULTILINE | re.DOTALL)
preg_pythontex = re.compile(r'^[^%\r\n]*?\\usepackage(\[[^{a]*\])?{pythontex}', re.MULTILINE | re.DOTALL)

target = sys.argv[1]

def process(filename):
    global SVGFILES, BIBLATEX_BACKEND, BIBFILES, INCLUDES, PYTHONTEX
    if os.path.isfile(filename):
        with open(filename) as f:
            content = f.read()
            SVGFILES += cleanup(preg_svg.findall(content), suffix='.pdf')

            backends = preg_biblatex_backend.findall(content)
            if backends and backends[-1]:
                BIBLATEX_BACKEND = backends[-1]

            BIBFILES += cleanup(preg_bibfiles.findall(content), suffix='.bib', key=lambda x: x[1])
            INCLUDES += cleanup(preg_includes.findall(content), suffix='.tex', key=lambda x: x[1])

            if preg_pythontex.search(content):
                PYTHONTEX = True

def cleanup(match_list, suffix=None, key=lambda x: x):
    items = [item
             for match in match_list
             for item in [item.strip()
                          for line in key(match).splitlines()
                          for item in line.split('%', 1)[0].split(',')]
             if item]

    if suffix is not None:
        items = [(item[:-len(suffix)] if item.endswith(suffix) else item) + suffix for item in items]

    return items

process(target)
for fname in INCLUDES:
    process(fname)

with open('dependencies.in', 'w') as f:
    svgtargets = [os.path.join(SVGBUILDDIR, svg) for svg in SVGFILES]
    f.write("SVGTARGETS = {}\n".format(' '.join(svgtargets)))
    f.write("BIBLATEX = {}\n".format(BIBLATEX_BACKEND))
    f.write("BIBFILES = {}\n".format(' '.join(BIBFILES)))
    f.write("INCLUDEDTEX = {}\n".format(' '.join(INCLUDES)))
    f.write("PYTHONTEX = {}\n".format('True' if PYTHONTEX else ''))
