#!/usr/bin/env python3
# coding: utf-8

# http://stackoverflow.com/a/19790001/1525423
# add reomte directory to package search path

import sys, os.path

here = os.path.dirname(os.path.abspath(__file__))
src = '../py'

sys.path.insert(0, os.path.normpath(os.path.join(here, src)))

# actually '../py/welle_fkm.py'
from welle_fkm import wA, wB
# from welle_mette import wA, wB
