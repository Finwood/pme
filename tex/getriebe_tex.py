#!/usr/bin/env python3
# coding: utf-8

# http://stackoverflow.com/a/19790001/1525423
# add reomte directory to package search path

import sys, os.path

here = os.path.dirname(os.path.abspath(__file__))
src = '../py'

sys.path.insert(0, os.path.normpath(os.path.join(here, src)))

# actually '../py/getriebe.py'
from getriebe import s1, s2, s3, w1, w2, w3, w4
from getriebe import L1, L2, L3, L4, L5, L6, L7, L8
import fkm_getriebe
