#!/bin/bash

# make: force rebuild

# if [ -z $DRAFT ]; then DRAFT=0; fi
DRAFT=0

if [ $1 ]
then

    REVISION=$(git rev-parse --short HEAD 2>/dev/null)
    GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
    GIT_DESCRIBE=$(git describe --dirty 2>/dev/null)

cat << EOF > $1
\\newcount\\isdraft\\isdraft=$DRAFT
\\newcommand{\\ifisdraft}[1]{\\ifnum\\isdraft>0 {#1} \\fi}

\\newcommand{\\gitRev}{$REVISION}
\\newcommand{\\gitBranch}{$GIT_BRANCH}
\\newcommand{\\gitDescribe}{$GIT_DESCRIBE}
EOF

fi
