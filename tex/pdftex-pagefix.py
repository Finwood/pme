#!/usr/bin/env python3
# coding: utf-8
"""
Inplace-fix of inkscape bug 1417470 (https://bugs.launchpad.net/inkscape/+bug/1417470)
Remove import of non-existing pdf pages

Usage:
    pdftex-pagefix.py <files>...

"""

import os
import re

from docopt import docopt
from PyPDF2 import PdfFileReader

def get_preg(file_stem):
    base = os.path.basename(file_stem)
    preg_str = r'.*\\includegraphics\[.*?page=(\d+).*?\]\{%s.pdf\}.*' % (base, )
    return re.compile(preg_str, re.DOTALL | re.MULTILINE)

def get_page_count(pdf_file_name):
    with open(pdf_file_name, 'rb') as pdf_file:
        pdf = PdfFileReader(pdf_file)
        return pdf.numPages

def fix_page_includes(file_stem):
    """Takes a file stem (without .pdf or .pdf_tex) and removes broken includes"""
    pdf_file = file_stem + '.pdf'
    pdf_tex_file = file_stem + '.pdf_tex'
    backup_file = pdf_tex_file + '~'

    total_pages = get_page_count(pdf_file)
    print("{}: {} pages".format(pdf_file, total_pages))
    preg = get_preg(file_stem)

    with open(pdf_tex_file) as fin:
        lines = fin.readlines()
    os.rename(pdf_tex_file, backup_file)

    try:
        with open(pdf_tex_file, 'w') as fout:
            for line in lines:
                match = preg.search(line)
                if match:
                    try:
                        page = int(match.group(1))
                    except (ValueError, TypeError):
                        pass
                    else:
                        if page > total_pages:
                            # This page reference is broken, ignore it silently
                            print("skip page {}".format(page))
                            continue
                fout.write(line)
    except:
        os.rename(backup_file, pdf_tex_file)
        raise
    else:
        os.remove(backup_file)

if __name__ == '__main__':
    args = docopt(__doc__, version='0.1')
    print(args)
    stems = set((f.rsplit('.', 1)[0] for f in args.get('<files>', [])))
    print(sorted(stems))
    for stem in sorted(stems):
        try:
            fix_page_includes(stem)
        except Exception as error:
            raise
            print(error)
            continue
