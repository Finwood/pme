# coding: utf-8

from . import s1, s2
import numpy as np
from numpy import vectorize

e3 = 24e-3
e4 = 120e-3
e5 = 27e-3

l3 = e3 + s1.b / 2
l4 = s1.b / 2 + e4 + s2.b / 2
l5 = s2.b / 2 + e5
L = l3 + l4 + l5

zahnräder = [(e3, e3+s1.b),
             (e3+s1.b+e4, L-e5)]


Fl3x = s1.Fa
Fl3y = - (s1.Fr * (l4 + l5) + s2.Fr * l5 - s1.Fa * s1.d2 / 2) / L
Fl3z = (-s1.Ft * (l4 + l5) + s2.Ft * l5) / L

Fl4x = 0
Fl4y = - (s1.Fr * l3 + s2.Fr * (l3 + l4) + s1.Fa * s1.d2 / 2) / L
Fl4z = (-s1.Ft * l3 + s2.Ft * (l3 + l4)) / L

Fl3r = np.hypot(Fl3y, Fl3z)
Fl4r = np.hypot(Fl4y, Fl4z)


@vectorize
def Mt(x):
    if l3 <= x < l3 + l4:
        return s1.M2
    return 0

@vectorize
def N(x):
    if 0 <= x < l3:
        return -Fl3x
    return 0

@vectorize
def Qy(x):
    if 0 <= x < l3:
        return -Fl3y
    elif l3 <= x < l3 + l4:
        return Fl4y + s2.Fr
    elif l3 + l4 <= x < L:
        return Fl4y
    return 0

@vectorize
def Qz(x):
    if 0 <= x < l3:
        return -Fl3z
    elif l3 <= x < l3 + l4:
        return Fl4z - s2.Ft
    elif l3 + l4 <= x < L:
        return Fl4z
    return 0

@vectorize
def My(x):
    if 0 <= x < l3:
        return -Fl3z * x
    elif l3 <= x < l3 + l4:
        return Fl4z * (x - L) - s2.Ft * (x - (l3 + l4))
    elif l3 + l4 <= x < L:
        return Fl4z * (x - L)
    return 0

@vectorize
def Mz(x):
    if 0 <= x < l3:
        return Fl3y * x
    elif l3 <= x < l3 + l4:
        return -Fl4y * (x - L) - s2.Fr * (x - (l3 + l4))
    elif l3 + l4 <= x < L:
        return -Fl4y * (x - L)
    return 0
