# coding: utf-8

import json
import yaml
import numpy as np


class Pressverband(object):
    Mt, Fa = 0, 0
    muH = .14  # me1, S. 335
    E = 210e9  # tbm, S. 46
    alpha = 11.9e-6  # tbm, S. 51
    jR, jF = 2, 1.7  # willkürlich
    RzW, RzN = 10e-6, 10e-6  # willkürlich, tbm S. 101
    theta_0 = 20

    passung_W, es, ei = '-', 0, 0
    passung_N, ES, EI = '-', 0, 0

    material = "20 MoCr 4"
    Rm, Re = 900e6, 620e6  # me2, Anhang Werkstoffkennwerte

    ATTRS = ['d', 'D', 'b', 'Mt', 'Fa', 'RzW', 'RzN', 'jR', 'jF',
             'Rm', 'Re', 'E', 'muH']

    @classmethod
    def from_file(cls, fname):
        for loader in (json.load, yaml.load):
            try:
                with open(fname) as f:
                    params = loader(f)
                return cls(**params)
            except (ValueError, TypeError, UnicodeDecodeError,
                    json.JSONDecodeError, yaml.scanner.ScannerError):
                pass
        raise ValueError("File not understood")

    def __init__(self, *args, **kwargs):
        for key, value in zip(self.ATTRS, args):
            object.__setattr__(self, key, value)
        for key, value in kwargs.items():
            object.__setattr__(self, key, value)

    @property
    def Fres(self):
        return np.hypot(self.Fa, self.Mt / (self.d / 2))
    @property
    def pF_erf(self):
        return self.jR * self.Fres / (self.muH * np.pi * self.d * self.b)

    @property
    def sigma_zul(self):
        return self.Re / self.jF
    @property
    def QN(self):
        return self.d / self.D
    @property
    def pF_zul(self):
        return self.sigma_zul * (1 - self.QN**2) / 2

    @property
    def G(self):
        return .8 * (self.RzW + self.RzN)
    def P(self, pF):
        return pF * self.d / self.E * 2 / (1 - self.QN**2) + self.G
    @property
    def Perf(self):
        return self.P(self.pF_erf)
    @property
    def Pzul(self):
        return self.P(self.pF_zul)

    @property
    def Pmin(self):
        return (self.ei - self.ES) * 1e-6
    @property
    def Pmax(self):
        return (self.es - self.EI) * 1e-6
    @property
    def PWmin(self):
        return self.Pmin - self.G
    @property
    def PWmax(self):
        return self.Pmax
    def pF(self, P):
        return P * self.E / self.d * (1 - self.QN**2) / 2
    @property
    def pF_min(self):
        return self.pF(self.PWmin)
    @property
    def pF_max(self):
        return self.pF(self.PWmax)
    def F(self, pF):
        return pF * self.muH * np.pi * self.d * self.b
    @property
    def Fast(self):
        return self.F(self.pF_min)
    @property
    def jBR(self):
        return self.Fast / self.Fres

    def passung_tex(self, name, upper, lower):
        d = self.d / 1e-3
        return r"\text{{\textsf{{{}}}}}:~^{{{:+.0f}}}_{{{:+.0f}}}". \
                format(name, upper, lower)
    @property
    def passung_N_tex(self):
        return self.passung_tex(self.passung_N, self.ES, self.EI)
    @property
    def passung_W_tex(self):
        return self.passung_tex(self.passung_W, self.es, self.ei)

    @property
    def Sri(self):
        return - self.pF_max
    @property
    def Sphii(self):
        return self.pF_max * (1 + self.QN**2) / (1 - self.QN**2)
    Sra = 0
    @property
    def Sphia(self):
        return self.pF_max * (2 * self.QN**2) / (1 - self.QN**2)
    @property
    def It(self):
        return np.pi / 32 * self.D**4
    @property
    def Tt(self):
        return self.Mt / self.It * self.d / 2
    @property
    def Svi(self):
        return np.sqrt(self.Sri**2 + self.Sphii**2 - self.Sra * self.Sphia
                       + 3 * self.Tt)
    @property
    def jN(self):
        return self.Re / self.Svi

    @property
    def Pstheta(self):
        return self.d * 1e-3
    @property
    def PUF(self):
        return self.PWmax + self.Pstheta
    @property
    def theta_erf(self):
        return self.theta_0 + self.PUF / self.alpha / self.d
