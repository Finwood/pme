# coding: utf-8

import re
import numpy as np
from tools import msg, user
import json
import yaml


class Lager(object):
    ausfallrisiko = 10.
    ATTRS = ['d', 'D', 'B', 'C', 'C0', 'Cu', 'Fr', 'Fa', 'n',
             'f0', 'e', 'X', 'Y', 'ec', 'nu', 'nu1', 'a1', 'aiso',
             'ausfallrisiko', 'name']
    A1 = {10.: 1, 5.: .64, 4.: .55, 3.: .47, 2.: .37, 1.: .25}

    @classmethod
    def from_file(cls, fname):
        for loader in (json.load, yaml.load):
            try:
                with open(fname) as f:
                    params = loader(f)
                if 'cls' in params:
                    cls = params['cls']
                    del params['cls']
                return cls(**params)
            except (ValueError, TypeError, UnicodeDecodeError,
                    json.JSONDecodeError, yaml.scanner.ScannerError):
                pass
            except AssertionError:
                raise TypeError("Bearing at '{}' is of type '{}', expected '{}'" \
                        .format(fname, params['cls'].__name__, cls.__name__))
        raise ValueError("File not understood")

    def __init__(self, name, *args, **kwargs):
        self.name = name
        for key, value in zip(self.ATTRS, args):
            object.__setattr__(self, key, value)
        for key, value in kwargs.items():
            object.__setattr__(self, key, value)

    @property
    def dm(self):
        return (self.d + self.D) / 2

    def __getattr__(self, key):
        if key == 'f0':
            self.f0 = user("f_0 für '{}' (S. 212)".format(self.name),
                           dtype=float)
            return self.f0
        if key in ('e', 'X', 'Y'):
            print("f_0 * F_a / C_0r = {:.1f} (S. 211)" \
                    .format(self.f0 * self.Fa / self.C0))
            self.e = user("e", dtype=float)
            self.X = user("X", dtype=float)
            self.Y = user("Y", dtype=float)
            return getattr(self, key)
        if key == 'ec':
            op = '<' if self.dm < 100 else '>='
            self.ec = user("e_c für d_M {} 100 mm (S. 50)".format(op), float)
            return self.ec
        if key == 'nu1':
            self.nu1 = user("nu_1 für d_M = {:.0f} mm "
                            "und n = {:.0f} / min (S. 45)" \
                                    .format(self.dm, self.n),
                            float)
            return self.nu1
        if key == 'nu':
            self.nu = user("nu (S. 45)", float)
            return self.nu
        if key == 'a1':
            p = float(self.ausfallrisiko)
            if p in self.A1:
                self.a1 = self.A1[p]
            else:
                self.a1 = user("a_1 für Ausfallrisiko {:.0f}% (S. 46)" \
                                .format(p), float)
                self.A1[p] = self.a1
            return self.a1
        if key == 'aiso':
            self.aiso = user("a_iso für e_c * C_u / P = {:.2f} "
                             "und kappa = {:.2f} (S. 49)" \
                                     .format(self.ec * self.Cu / self.P,
                                             self.kappa),
                             float)
            return self.aiso
        match = re.match(r'L(?P<n>\d+)m(?P<h>h?)', key)
        if match:
            n = match.group('n')
            n = int(n if not n.startswith('0') else n / 10)
            if match.group('h'):
                return self.Lnmh(n)
            else:
                return self.Lnm(n)
        if key in self.ATTRS:
            success, val = user("Attribute '{}' needed".format(key), float,
                                allow_interrupt=True)
            if success:
                setattr(self, key, val)
                return val
            return
        raise AttributeError("type object '{}' has no attribute '{}'" \
                .format(self.__class__.__name__, key))

    def __setattr__(self, key, value):
        if isinstance(value, np.generic):
            value = value.item()
        if key in ('C', 'C0', 'Cu', 'Fr', 'Fa'):
            value = abs(value)
            if value > 1e3:
                value /= 1e3
        if key in ('ec', 'C', 'Cu', 'Fr', 'Fa', 'nu', 'nu1'):
            try:
                del self.aiso
            except AttributeError:
                pass
        object.__setattr__(self, key, value)

    @property
    def P(self):
        """Dynamisch äquivalente Lagerbelastung, S.211"""
        if not hasattr(self, 'Fa') or not self.Fa:
            return self.Fr
        if self.Fa / self.Fr <= self.e:
            return self.Fr
        else:
            return self.X * self.Fr + self.Y * self.Fa
    @property
    def P0(self):
        """Statisch äquivalente Lagerbelastung, S.213"""
        if not hasattr(self, 'F0a') or not self.F0a:
            return self.F0r
        if self.F0a / self.F0r <= .8:
            return self.F0r
        else:
            return .6 * self.F0r + .5 * self.F0a

    def _Lh(self, L):
        return L * 16666 / self.n
    @property
    def L10(self):
        return (self.C / self.P)**self.p
    @property
    def L10h(self):
        return self._Lh(self.L10)

    def Lnm(self, n):
        if n != self.ausfallrisiko and hasattr(self, 'a1'):
            del self.a1
        self.ausfallrisiko = n
        return self.a1 * self.aiso * self.L10
    def Lnmh(self, n):
        return self._Lh(self.Lnm(n))
    @property
    def kappa(self):
        return self.nu / self.nu1

    @property
    def data(self):
        return {key: getattr(self, key)
                for key in dir(self)
                if key in self.ATTRS}

    def __repr__(self):
        kwstring = ', '.join(('{}={}'.format(key, val))
                             for key, val in self.data.items())
        return "{}({})".format(self.__class__.__name__, kwstring)

    def save(self, fname=None):
        if fname is None:
            fname = '{}.yaml'.format(self.name)
        data = self.data
        data['cls'] = self.__class__
        with open(fname, 'w') as f:
            if fname.endswith('.json'):
                json.dump(data, f, indent=4)
            else:
                yaml.dump(data, f, default_flow_style=False)
        return fname

    def nu_for(self, Lhsoll):
        aiso = round(Lhsoll / self.L10h / self.a1, 2)
        kappa = user("kappa für e_c * C_u / P = {:.2f} "
                     "und a_iso >= {:.2f} (S. 49)" \
                             .format(self.ec * self.Cu / self.P, aiso),
                     float)
        nu1 = self.nu1
        nu = kappa * nu1
        save, my_nu = user("nu >= {:.0f} (S. 45)".format(nu), float, True)
        if save:
            self.nu = nu = my_nu
        return nu


class Kugellager(Lager):
    p = 10 / 3  # S. 42


class Rollenlager(Lager):
    p = 3  # S. 42
