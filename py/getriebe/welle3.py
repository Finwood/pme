# coding: utf-8

from . import s2, s3
import numpy as np
from numpy import vectorize

e6 = 27e-3
e7 = 120e-3
e8 = 30e-3

l6 = e6 + s2.b / 2
l7 = s2.b / 2 + e7 + s3.b / 2
l8 = s3.b / 2 + e8
L = l6 + l7 + l8

zahnräder = [(e6, e6+s2.b),
             (e6+s2.b+e7, L-e8)]


Fl5x = 0
Fl5y = (s2.Fr * (l7 + l8) + s3.Fr * l8) / L
Fl5z = (-s2.Ft * (l7 + l8) + s3.Ft * l8) / L

Fl6x = 0
Fl6y = (s2.Fr * l6 + s3.Fr * (l6 + l7)) / L
Fl6z = (-s2.Ft * l6 + s3.Ft * (l6 + l7)) / L

Fl5r = np.hypot(Fl5y, Fl5z)
Fl6r = np.hypot(Fl6y, Fl6z)


@vectorize
def Mt(x):
    if l6 <= x < l6 + l7:
        return -s2.M2
    elif l6 + l7 <= x:
        return -(s2.M2 - s3.M1)
    return 0

@vectorize
def N(x):
    return 0

@vectorize
def Qy(x):
    if 0 <= x < l6:
        return -Fl5y
    elif l6 <= x < l6 + l7:
        return -Fl5y + s2.Fr
    elif l6 + l7 <= x < L:
        return Fl6y
    return 0

@vectorize
def Qz(x):
    if 0 <= x < l6:
        return -Fl5z
    elif l6 <= x < l6 + l7:
        return -Fl5z - s2.Ft
    elif l6 + l7 <= x < L:
        return Fl6z
    return 0

@vectorize
def My(x):
    if 0 <= x < l6:
        return -Fl5z * x
    elif l6 <= x < l6 + l7:
        return -Fl5z * x - s2.Ft * (x - l6)
    elif l6 + l7 <= x < L:
        return Fl6z * (x - L)
    return 0

@vectorize
def Mz(x):
    if 0 <= x < l6:
        return Fl5y * x
    elif l6 <= x < l6 + l7:
        return Fl5y * x - s2.Fr * (x - l6)
    elif l6 + l7 <= x < L:
        return -Fl6y * (x - L)
    return 0
