#!/usr/bin/env python3

from os.path import realpath, dirname, join
PYROOT = realpath(dirname(__file__))

from .verzahnung import Stufe

M_W = 13.33e3
n_an = 525

s1 = Stufe.from_file(join(PYROOT, 's1.yaml'))
s2 = Stufe.from_file(join(PYROOT, 's2.yaml'))
s3 = Stufe.from_file(join(PYROOT, 's3.yaml'))

s3.M2 = M_W
s2.M2 = M_W + s3.M1
s1.M2 = s2.M1

s1.n1 = n_an
s2.n1 = s1.n2
s3.n1 = s2.n2


from . import welle1 as w1
from . import welle2 as w2
from . import welle3 as w3
from . import welle4 as w4


from .lager import Lager, Kugellager, Rollenlager
def _update_value(L, key, val, precision=2):
    if round(getattr(L, key), precision) != round(val, precision):
        print("Updating {} to {}".format(key, val))
        setattr(L, key, val)

L1 = Lager.from_file(join(PYROOT, 'L1.yaml'))
L2 = Lager.from_file(join(PYROOT, 'L2.yaml'))
L3 = Lager.from_file(join(PYROOT, 'L3.yaml'))
L4 = Lager.from_file(join(PYROOT, 'L4.yaml'))
L5 = Lager.from_file(join(PYROOT, 'L5.yaml'))
L6 = Lager.from_file(join(PYROOT, 'L6.yaml'))
L7 = Lager.from_file(join(PYROOT, 'L7.yaml'))
L8 = Lager.from_file(join(PYROOT, 'L8.yaml'))


for L, Fr, Fa, n in [(L1, w1.Fl1r, w1.Fl1x, s1.n1),
                     (L2, w1.Fl2r, w1.Fl2x, s1.n1),
                     (L3, w2.Fl3r, w2.Fl3x, s2.n1),
                     (L4, w2.Fl4r, w2.Fl4x, s2.n1),
                     (L5, w3.Fl5r, w3.Fl5x, s3.n1),
                     (L6, w3.Fl6r, w3.Fl6x, s3.n1),
                     (L7, w4.Fl7r, w4.Fl7x, s3.n2),
                     (L8, w4.Fl8r, w4.Fl8x, s3.n2)]:
    _update_value(L, 'Fr', abs(Fr) / 1e3)
    _update_value(L, 'Fa', abs(Fa) / 1e3)
    _update_value(L, 'n', n)


from .welle_nabe import Pressverband

pv1 = Pressverband(60e-3, s1.d1, s1.b, s1.M1, s1.Fa,
                   passung_W='u6', es=106, ei=87,
                   passung_N='H8', ES=46, EI=0)
pv2 = Pressverband(80e-3, s1.d2, s1.b, s1.M2, s1.Fa,
                   passung_W='u8', es=148, ei=102,
                   passung_N='H7', ES=30, EI=0)
pv3 = Pressverband(90e-3, s2.d1, s2.b, s2.M1, s2.Fa,
                   passung_W='u7', es=159, ei=124,
                   passung_N='H7', ES=35, EI=0)
pv4 = Pressverband(110e-3, s2.d2, s2.b, s2.M2, s2.Fa,
                   passung_W='v6', es=194, ei=172,
                   passung_N='H5', ES=15, EI=0)
pv5 = Pressverband(110e-3, s3.d1, s3.b, s3.M1, s3.Fa,
                   passung_W='u8', es=198, ei=144,
                   passung_N='H6', ES=22, EI=0)
