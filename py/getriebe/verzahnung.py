#!/usr/bin/env python3

import numpy as np
from tools import user, hr
import pickle
import json
import yaml


def inv(alpha, deg=False):
    if deg:
        alpha = np.radians(alpha)
    return np.tan(alpha) - alpha

def arcinv(a):
    return (3 * a)**(1/3) - 2/5 * a


class Stufe(object):
    alpha = np.radians(20)
    beta = 0

    K_AB = 1  # Anwendungsfaktor, F.389
    K_v = 1.1  # Dynamikfaktor, F.394
    Y_F = 2.2  # Zahnformfaktor, F.400
    Y_S = 1.7  # Spannungskorrekturfaktor, F.402
    K_Fbeta = 1.25  # Breitlastverteilungsfaktor, F.404
    nu_F = 1.7  # Sicherheit Zahnfuß, F.405

    Z_E = 190e3  # Elastizitätsfaktor, F.410  [sqrt(MPa) = sqrt(Pa) * 1e3]
    K_Hbeta = 1.50  # Breitlastverteilungsfaktor, F.413
    nu_H = 1.25  # Sicherheit Zahnflanke, F.414

    x1 = 0
    x2 = 0

    # Werkstoff: 20MoCr4
    sigma_Flim = 400e6  # 400 MPa
    sigma_Hlim = 1630e6  # 1630 MPa

    eta = .99
    c_rel = .25  # Kopfkürzungsfaktor, DIN 867

    def __init__(self, z1, z2, mn, b, M1=0, n1=0, **kw):
        self.z1 = z1
        self.z2 = z2
        self.b = b if b < 1 else b / 1e3
        self.mn = mn if mn < 1 else mn / 1e3
        self.M1 = M1
        self.n1 = n1
        for key, val in kw.items():
            setattr(self, key, val)

    @property
    def i(self):
        return self.z2 / self.z1
    @property
    def Z(self):
        return self.z1 + self.z2

    @property
    def mt(self):
        return self.mn / np.cos(self.beta)
    @property
    def a0(self):
        return self.mn * self.Z / (2 * np.cos(self.beta))
    @property
    def a(self):
        if hasattr(self, '_a'):
            return self._a
        return self.a0
    @a.setter
    def a(self, value):
        self._a = value
    @property
    def c(self):
        return self.c_rel * self.mn

    @property
    def alpha_t(self):
        return np.arctan(np.tan(self.alpha) / np.cos(self.beta))
    @property
    def alpha_w(self):
        return arcinv(inv(self.alpha) + 2 * self.X * np.tan(self.alpha) / \
                                        self.Z * np.cos(self.beta)**3)
    @property
    def alpha_wt(self):
        return np.arccos(self.mn * self.Z / (2 * self.a) * \
                         np.cos(self.alpha_t) / np.cos(self.beta))
    @property
    def beta_b(self):
        return np.arcsin(np.sin(self.beta) * np.cos(self.alpha))
    @property
    def alpha_a1(self):
        return np.arccos(self.db1 / self.da1)
    @property
    def alpha_a2(self):
        return np.arccos(self.db2 / self.da2)
    @property
    def X(self):
        return self.Z * (inv(self.alpha_wt) - inv(self.alpha_t)) / \
                (2 * np.tan(self.alpha_t))
    @property
    def k(self):
        return self.X - self.Z / (2 * np.cos(self.beta)) * \
                        (np.cos(self.alpha_t) - np.cos(self.alpha_wt)) / \
                        np.cos(self.alpha_wt)

    @property
    def hfP(self):
        return self.mn + self.c
    @property
    def hf1(self):
        return self.hfP - self.x1 * self.mn
    @property
    def hf2(self):
        return self.hfP - self.x2 * self.mn

    @property
    def d1(self):
        return(self.mt * self.z1)
    @property
    def d2(self):
        return(self.mt * self.z2)

    @property
    def da1(self):
        return 2 * self.a - self.df2 - 2 * self.c
    @property
    def da2(self):
        return 2 * self.a - self.df1 - 2 * self.c

    @property
    def df1(self):
        return self.d1 - 2 * self.hf1
    @property
    def df2(self):
        return self.d2 - 2 * self.hf2

    @property
    def db1(self):
        return self.d1 * np.cos(self.alpha_t)
    @property
    def db2(self):
        return self.d2 * np.cos(self.alpha_t)

    @property
    def dv1(self):
        return self.d1 + 2 * self.x1 * self.mn
    @property
    def dv2(self):
        return self.d2 + 2 * self.x2 * self.mn

    @property
    def dw1(self):
        return self.db1 / np.cos(self.alpha_wt)
    @property
    def dw2(self):
        return self.db2 / np.cos(self.alpha_wt)

    @property
    def ga(self):
        return (np.sqrt(self.da1**2 - self.db1**2)
                - self.db1 * np.tan(self.alpha_wt)) / 2
    @property
    def gf(self):
        return (np.sqrt(self.da2**2 - self.db2**2)
                - self.db2 * np.tan(self.alpha_wt)) / 2
    @property
    def g_alpha(self):
        return self.ga + self.gf
    @property
    def pet(self):
        return np.pi * self.mt * np.cos(self.alpha_t)
    @property
    def epsilon_alpha(self):
        return self.g_alpha / self.pet
    @property
    def epsilon_beta(self):
        return self.b * np.sin(self.beta) / (np.pi * self.mn)
    @property
    def epsilon_gamma(self):
        return self.epsilon_alpha + self.epsilon_beta

    @property
    def zn1(self):
        return self.z1 / np.cos(self.beta)**3
    @property
    def zn2(self):
        return self.z2 / np.cos(self.beta)**3

    @property
    def Y_epsilon(self):
        if hasattr(self, '_Y_epsilon'):
            return self._Y_epsilon
        return 1 / self.epsilon_alpha
    @Y_epsilon.setter
    def Y_epsilon(self, value):
        self._Y_epsilon = value

    @property
    def Ft(self):
        return self.M1 / (self.d1 / 2)
    @property
    def Fr(self):
        return self.Ft * np.tan(self.alpha_t)
    @property
    def Fa(self):
        return self.Ft * np.tan(self.beta)
    @property
    def M2(self):
        return self.i * self.M1 * self.eta
    @M2.setter
    def M2(self, value):
        self.M1 = value / (self.i * self.eta)
    @property
    def n2(self):
        return self.n1 / self.i
    @n2.setter
    def n2(self, value):
        self.n1 = value * self.i

    @property
    def sigma_Fzul(self):
        return self.sigma_Flim / self.nu_F
    @property
    def sigma_Hzul(self):
        return self.sigma_Hlim / self.nu_H

    @property
    def sigma_F1(self):
        K = self.K_AB * self.K_v * self.K_Fbeta
        Y = self.Y_F * self.Y_S * self.Y_epsilon
        geometry = self.d1 / 2 * self.b * self.mn
        return self.M1 * K / geometry * Y
    @property
    def sigma_F2(self):
        K = self.K_AB * self.K_v * self.K_Fbeta
        Y = self.Y_F * self.Y_S * self.Y_epsilon
        geometry = self.d2 / 2 * self.b * self.mn
        return self.M2 * K / geometry * Y

    @property
    def Z_H(self):
        a = 2 * np.cos(self.beta_b) * np.cos(self.alpha_wt)
        b = np.cos(self.alpha_t)**2 * np.sin(self.alpha_wt)
        return np.sqrt(a/b)
    @property
    def Z_epsilon(self):
        eb = self.epsilon_beta
        ea = self.epsilon_alpha
        if eb >= 1:
            return np.sqrt(1/ea)
        else:
            return np.sqrt((4-ea) * (1-eb) / 3 + eb/ea)
    @property
    def Z_beta(self):
        return np.sqrt(np.cos(self.beta))

    @property
    def sigma_H1(self):
        Z = self.Z_E * self.Z_H * self.Z_epsilon * self.Z_beta
        K = self.K_AB * self.K_v * self.K_Hbeta
        i = self.i
        geo = self.d1 * self.b
        return Z * np.sqrt((i+1)/i) * np.sqrt(self.Ft * K / geo)
    @property
    def sigma_H2(self):
        Z = self.Z_E * self.Z_H * self.Z_epsilon * self.Z_beta
        K = self.K_AB * self.K_v * self.K_Hbeta
        i = self.i
        geo = self.d2 * self.b
        return Z * np.sqrt((i+1)/i) * np.sqrt(self.Ft * K / geo)

    @property
    def psi1(self):
        return np.tan(self.alpha_a1) / np.tan(self.alpha_wt)
    @property
    def psi2(self):
        return np.tan(self.alpha_a2) / np.tan(self.alpha_wt)
    @property
    def zeta1(self):
        psi = self.psi2
        i = self.i
        return (1 - psi) / (1 - i / (i+1) * psi)
    @property
    def zeta2(self):
        psi = self.psi1
        i = self.i
        return (1 - psi) / (1 - 1 / (i+1) * psi)

    @property
    def valid(self):
        return all((self.epsilon_alpha >= 1.25,
                    not self.beta or self.epsilon_beta >= 1,
                    self.sigma_F1 <= self.sigma_Fzul,
                    self.sigma_F2 <= self.sigma_Fzul,
                    self.sigma_H1 <= self.sigma_Hzul,
                    self.sigma_H2 <= self.sigma_Hzul,
                    -2.5 <= self.zeta1 <= -.5,
                    -2.5 <= self.zeta2 <= -.5))

    def interact(self):
        self.a = user("Achsabstand für a0 = {}m [mm]" \
                .format(hr(self.a0, fmt='.2f')), dtype=int) * 1e-3
        print("Profilverschiebungssumme X = {:.2f}".format(self.X))
        print("Aufteilen auf z1 = {} und z2 = {} (F.400)" \
                .format(self.z1, self.z2))
        self.x1 = user("Profilverschiebung x1", dtype=float)
        self.x2 = self.X - self.x1
        print("Profilverschiebung x2 = {:.2f}".format(self.x2))
        print("Profilüberdeckung epsilon_alpha = {:.2f}" \
                .format(self.epsilon_alpha))
        print("Sprungüberdeckung epsilon_beta = {:.2f}" \
                .format(self.epsilon_beta))

        print("Nachweis Zahnfußfestigkeit")
        if self.beta:
            print("Profilverschiebungssumme X = {:.2f} aufteilen auf" \
                    .format(self.X))
            print("Ersatzzähnezahlen zn1 = {:.1f} und zn2 = {:.1f} (F.400)" \
                    .format(self.zn1, self.zn2))
        self.Y_F = user("Zahnformfaktor Y_F (F. 400)", dtype=float)
        self.Y_S = user("Spannungskorrekturfaktor Y_S (F. 402)", dtype=float)

    def save(self, fname):
        with open(fname, 'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def from_file(cls, fname):
        try:
            with open(fname, 'rb') as f:
                obj = pickle.load(f)
            if isinstance(obj, cls):
                return obj
        except pickle.UnpicklingError:
            for loader in (json.load, yaml.load):
                try:
                    with open(fname) as f:
                        params = loader(f)
                    return cls(**params)
                except (ValueError, TypeError, UnicodeDecodeError,
                        json.JSONDecodeError, yaml.scanner.ScannerError):
                    pass
        raise ValueError("File not understood")

if __name__ == '__main__':
    beta=np.radians(10)
    s = Stufe(18, 151, 5.5, 105, 571.34, beta=beta)
    s.interact()

    while True:
        try:
            key = input("> ")
        except (KeyboardInterrupt, EOFError):
            break
        if hasattr(s, key):
            print(getattr(s, key))
            print()
        else:
            print("invalid key\n")
