# coding: utf-8

from . import s3
import numpy as np
from numpy import vectorize

e9 = 27e-3
e10 = 30e-3

l9 = e9 + s3.b / 2
l10 = s3.b / 2 + e10
L = l9 + l10

zahnräder = [(e9, e9+s3.b)]


Fl7x = 0
Fl7y = s3.Fr * l10 / L
Fl7z = -s3.Ft * l10 / L

Fl8x = 0
Fl8y = s3.Fr * l9 / L
Fl8z = -s3.Ft * l9 / L


Fl7r = np.hypot(Fl7y, Fl7z)
Fl8r = np.hypot(Fl8y, Fl8z)

@vectorize
def Mt(x):
    if l9 <= x:
        return s3.M2
    return 0

@vectorize
def N(x):
    return 0

@vectorize
def Qy(x):
    if 0 <= x < l9:
        return -Fl7y
    elif l9 <= x < L:
        return Fl8y
    return 0

@vectorize
def Qz(x):
    if 0 <= x < l9:
        return -Fl7z
    elif l9 <= x < L:
        return Fl8z
    return 0

@vectorize
def My(x):
    if 0 <= x < l9:
        return -Fl7z * x
    elif l9 <= x < L:
        return Fl8z * (x - L)
    return 0

@vectorize
def Mz(x):
    if 0 <= x < l9:
        return Fl7y * x
    elif l9 <= x < L:
        return -Fl8y * (x - L)
    return 0
