# coding: utf-8

from . import s1
import numpy as np
from numpy import vectorize

e1 = 24e-3
e2 = 24e-3

l1 = e1 + s1.b / 2
l2 = e2 + s1.b / 2
L = l1 + l2

zahnräder = [(e1, e1+s1.b)]


Fl1x = 0
Fl1y = (s1.Fr * l2 - s1.Fa * s1.d1 / 2) / L
Fl1z = s1.Ft * l2 / L

Fl2x = -s1.Fa
Fl2y = (s1.Fr * l1 + s1.Fa * s1.d1 / 2) / L
Fl2z = s1.Ft * l1 / L

Fl1r = np.hypot(Fl1y, Fl1z)
Fl2r = np.hypot(Fl2y, Fl2z)

@vectorize
def Mt(x):
    if x < l1:
        return -s1.M1
    return 0

@vectorize
def N(x):
    if l1 <= x < L:
        return -s1.Fa
    return 0

@vectorize
def Qy(x):
    if 0 <= x < l1:
        return -Fl1y
    elif l1 <= x < L:
        return Fl2y
    return 0

@vectorize
def Qz(x):
    if 0 <= x < l1:
        return -Fl1z
    elif l1 <= x < L:
        return Fl2z
    return 0

@vectorize
def My(x):
    if 0 <= x <= l1:
        return -Fl1z * x
    elif l1 <= x < L:
        return Fl2z * (x - L)
    return 0

@vectorize
def Mz(x):
    if 0 <= x < l1:
        return Fl1y * x
    elif l1 <= x < L:
        return -Fl2y * (x - L)
    return 0
