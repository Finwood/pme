# coding: utf-8
"""Einige vordefinierte Werkstoffe, optional"""

from . import Werkstoff

w_34CrMo4 = Werkstoff(name='34CrMo4',
        nummer='1.7220',
        Rm=1000e6,
        Re=800e6,
        S_Wzd=450e6,
        T_Ws=260e6,
        d_effNm=16e-3,
        d_effNp=16e-3,
        a_dm=0.30,
        a_dp=0.44)

w_50CrMo4 = Werkstoff(name='50CrMo4',
        nummer='1.7228',
        Rm=1100e6,
        Re=900e6,
        S_Wzd=495e6,
        T_Ws=285e6,
        d_effNm=16e-3,
        d_effNp=16e-3,
        a_dm=0.28,
        a_dp=0.38)

w_S235 = Werkstoff(name='S235JR',
        nummer='1.0037',
        Rm=360e6,
        Re=235e6,
        S_Wzd=160e6,
        T_Ws=95e6,
        d_effNm=40e-3,
        d_effNp=40e-3,
        a_dm=0.15,
        a_dp=0.30)
