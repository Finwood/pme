# coding: utf-8

import numpy as np

class Kerbe:
    """Basisklasse für Kerben.

    Erforderliche Attribute:
        Sonstiges:
            name    "human-readable" Bezeichnung der Kerbenart

        Formzahlen:
            Ktzd    Zug/Druck
            Ktb     Biegung
            Kts     Schub (unüblich)
            Ktt     Torsion

        Bezogene Spannungsgradienten:
            Gsd     Gradient aufgrund Geometrie bzgl. Normalspannung
            Gtd     Gradient aufgrund Geometrie bzgl. Schubspannung
            Gsr     Gradient aufgrund Kerbradius bzgl. Normalspannung
            Gtr     Gradient aufgrund Kerbradius bzgl. Schubspannung
    """
    Kfzd = 1
    Kfb = 1
    Kfs = 1
    Kft = 1
    name = "Kerbe"

    def __init__(self, Rm=None, aG=None, bG=None, **kw):
        """Rm ist die nach FKM/statisch berechnete korrigierte Zugfestigkeit im Bauteil"""
        self.Rm = Rm
        self.aG = aG or 0.5
        self.bG = bG or 2700

    @property
    def t(self):
        """Tiefe der Kerbe"""
        return (self.D - self.d) / 2

    def get_phi(self, **kw):
        """Rechengröße zur Bestimmung des von der Kerbform hervorgerufenen Spannungsgefälles"""
        t = kw.get('t') or self.t
        d = kw.get('d') or kw.get('b') or self.d
        r = kw.get('r') or self.r
        return 0 if t / d > 0.25 else 1 / (4 * np.sqrt(t / r) + 2)
    @property
    def phi(self): # wrapper
        """Rechengröße zur Bestimmung des von der Kerbform hervorgerufenen Spannungsgefälles"""
        return self.get_phi()

    def get_Gsd(self, **kw):
        """bezogener Spannungsgradient aufgrund der Geometrie für Normalspannungen, F.52"""
        return 2 / kw.get('d', self.d)
    @property
    def Gsd(self): # wrapper
        """bezogener Spannungsgradient aufgrund der Geometrie für Normalspannungen, F.52"""
        return self.get_Gsd()
    def get_Gtd(self, **kw):
        """bezogener Spannungsgradient aufgrund der Geometrie für Schubspannungen, F.52"""
        return 2 / kw.get('d', self.d)
    @property
    def Gtd(self): # wrapper
        """bezogener Spannungsgradient aufgrund der Geometrie für Schubspannungen, F.52"""
        return self.get_Gtd()
    @property
    def Gsr(self): # wrapper
        """bezogener Spannungsgradient aufgrund des Kerbradius für Schubspannungen, F.52"""
        return self.get_Gsr()
    @property
    def Gtr(self): # wrapper
        """bezogener Spannungsgradient aufgrund des Kerbradius für Normalspannungen, F.52"""
        return self.get_Gtr()

    def get_n(self, which, G, **kw):
        """Stützzahlen, alle auf einmal. F.54"""
        assert which.lower() in ('s', 't', 'sigma', 'tau'), "Ungültiger Aufruf, `which` sollte 's' oder 't' sein"
        f = 0.577 if which.lower()[0] == 't' else 1
        k = 0.5 if G <= 0.1 / 1e-3 else 0
        exponent = 10**-(kw.get('aG', self.aG) - k + (f * kw.get('Rm', self.Rm) / (kw.get('bG', self.bG) * 1e6)))
        if G <= 0.1 / 1e-3:
            base = G * 1e-3
        elif G <= 1 / 1e-3:
            base = np.sqrt(G * 1e-3)
        elif G <= 100 / 1e-3:
            base = (G * 1e-3)**(1/4)
        else:
            base = (100)**(1/4)
        return 1 + base * exponent
    def get_nsd(self, Gsd, **kw):
        """plastische Stützzahl in Abhängigkeit eines Gs(d), F.54"""
        return self.get_n('s', Gsd, **kw)
    def get_ntd(self, Gtd, **kw):
        """plastische Stützzahl in Abhängigkeit eines Gt(d), F.54"""
        return self.get_n('t', Gtd, **kw)
    def get_nsr(self, Gsr, **kw):
        """plastische Stützzahl in Abhängigkeit eines Gs(r), F.54"""
        return self.get_n('s', Gsr, **kw)
    def get_ntr(self, Gtr, **kw):
        """plastische Stützzahl in Abhängigkeit eines Gt(r), F.54"""
        return self.get_n('t', Gtr, **kw)
    @property
    def nsd(self): # wrapper
        """plastische Stützzahl für Gs(d), F.54"""
        return self.get_nsd(self.Gsd)
    @property # wrapper
    def ntd(self):
        """plastische Stützzahl für Gt(d), F.54"""
        return self.get_ntd(self.Gtd)
    @property
    def nsr(self): # wrapper
        """plastische Stützzahl für Gs(r), F.54"""
        return self.get_nsr(self.Gsr)
    @property
    def ntr(self): # wrapper
        """plastische Stützzahl für Gt(r), F.54"""
        return self.get_ntr(self.Gtr)

    def __bool__(self):
        return not all(k == 1 for k in (self.Kfzd, self.Kfb, self.Kfs, self.Kft))
    def __str__(self):
        return "{s.name}: {params}\n" \
               "    Kt,zd  = {s.Ktzd:.2f}\n" \
               "    Kt,b   = {s.Ktb:.2f}\n" \
               "    Kt,s   = {s.Kts:.2f}\n" \
               "    Kt,t   = {s.Ktt:.2f}\n" \
               "    Gs(d)  = {s.Gsd:.2f}\n" \
               "    Gt(d)  = {s.Gtd:.2f}\n" \
               "    Gs(r)  = {s.Gsr:.2f}\n" \
               "    Gt(r)  = {s.Gtr:.2f}\n".format(s=self, params=self._serialize())
    def _serialize(self):
        return ""

class KerbeKt(Kerbe):
    """Kerbe anhand Formzahldiagramm"""
    @property
    def Kfzd(self):
        """Kerbwirkungszahl bezüglich Zug/Druck, F.57"""
        return max(self.Ktzd / self.nsr, 1)
    @property
    def Kfb(self):
        """Kerbwirkungszahl bezüglich Biegung, F.57"""
        return max(self.Ktb / self.nsr, 1) / self.nsd
    @property
    def Kfs(self):
        """Kerbwirkungszahl bezüglich Schub, F.57"""
        return max(self.Kts / self.ntr, 1)
    @property
    def Kft(self):
        """Kerbwirkungszahl bezüglich Torsion, F.57"""
        return max(self.Ktt / self.ntr, 1) / self.ntd

class KerbeKf(Kerbe):
    """Kerbe anhand experimentell ermittelter Kerbwirkungszahlen"""
    @property
    def GsdP(self): # wrapper
        """bezogener Spannungsgradient aufgrund der Geometrie für Normalspannungen, F.52"""
        return self.get_Gsd(d=self.dP)
    @property
    def GtdP(self): # wrapper
        """bezogener Spannungsgradient aufgrund der Geometrie für Schubspannungen, F.52"""
        return self.get_Gtd(d=self.dP)
    @property
    def GsrP(self): # wrapper
        """bezogener Spannungsgradient aufgrund des Kerbradius für Normalspannungen, F.55"""
        return self.get_Gsr(phi=self.get_phi(t=self.tP, d=self.dP, r=self.rP), r=self.rP)
    @property
    def GtrP(self): # wrapper
        """bezogener Spannungsgradient aufgrund des Kerbradius für Schubspannungen, F.55"""
        return self.get_Gtr(r=self.rP)

    @property
    def nsdP(self): # wrapper
        """plastische Stützzahl für Gs(d), F.54"""
        return self.get_nsd(self.GsdP)
    @property # wrapper
    def ntdP(self):
        """plastische Stützzahl für Gt(d), F.54"""
        return self.get_ntd(self.GtdP)
    @property
    def nsrP(self): # wrapper
        """plastische Stützzahl für Gs(r), F.54"""
        return self.get_nsr(self.GsrP)
    @property
    def ntrP(self): # wrapper
        """plastische Stützzahl für Gt(r), F.54"""
        return self.get_ntr(self.GtrP)

    @property
    def Kfzd(self):
        """Kerbwirkungszahl bezüglich Zug/Druck, F.57"""
        Ktzd = self.KfzdP * self.nsrP
        return max(Ktzd / self.nsr, 1)
    @property
    def Kfb(self):
        """Kerbwirkungszahl bezüglich Biegung, F.57"""
        Ktb = self.KfbP * self.nsrP
        return max(Ktb / self.nsr, 1) / self.nsd
    @property
    def Kfs(self):
        """Kerbwirkungszahl bezüglich Schub, F.57"""
        Kts = self.KfsP * self.ntrP
        return max(Kts / self.ntr, 1)
    @property
    def Kft(self):
        """Kerbwirkungszahl bezüglich Torsion, F.57"""
        Ktt = self.KftP * self.ntrP
        return max(Ktt / self.ntr, 1) / self.ntd

class Wellenabsatz(KerbeKt):
    name = "Wellenabsatz"
    def __init__(self, **kw):
        r = kw.get('r')
        d = kw.get('d')
        D = kw.get('D')
        t = kw.get('t')
        assert r is not None and r > 0, "`r` has to be positive"
        self.r = r
        assert sum(map(bool, (d, D, t))) >= 2, "please specify 2 of (d, D, t)"
        if d is not None:
            self.d = d
        elif t is not None and D is not None:
            self.d = D - 2*t
        if D is not None:
            self.D = D
        elif t is not None and d is not None:
            self.D = d + 2*t
        assert self.d < self.D
        super().__init__(**kw)

    def _serialize(self):
        return "d = {s.d:.3e}, D = {s.D:.3e}, t = {s.t:.3e}, r = {s.r:.3e}".format(s=self)

    @property
    def Ktzd(self):
        """Formzahl bezüglich Zug/Druck"""
        return 1 + 1 / np.sqrt(.62*self.r/self.t + 7*self.r/self.d * (1 + 2*self.r/self.d)**2)
    @property
    def Ktb(self):
        """Formzahl bezüglich Biegung"""
        return 1 + 1 / np.sqrt(.62*self.r/self.t + 11.6*self.r/self.d * (1 + 2*self.r/self.d)**2 + .2 * (self.r/self.t)**3 * self.d/self.D)
    @property
    def Ktt(self):
        """Formzahl bezüglich Torsion"""
        return 1 + 1 / np.sqrt(3.4*self.r/self.t + 38*self.r/self.d * (1 + 2*self.r/self.d)**2 + 1 * (self.r/self.t)**3 * self.d/self.D)
    @property
    def Kts(self):
        """Formzahl bezüglich Schub"""
        return max(self.Ktzd, self.Ktb, self.Ktt)

    @property
    def Gsr(self):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Normalspannungen, F.55"""
        return 2.3 * (1 + self.phi) / self.r
    @property
    def Gtr(self):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Schubspannungen, F.55"""
        return 1.15 / self.r

class Umlaufkerbe(KerbeKt):
    name = "Umlaufkerbe"
    def __init__(self, **kw):
        r = kw.get('r')
        d = kw.get('d')
        D = kw.get('D')
        t = kw.get('t')
        assert r is not None and r > 0, "`r` has to be positive"
        self.r = r
        assert sum(map(bool, (d, D, t))) >= 2, "please specify 2 of (d, D, t)"
        if d is not None:
            self.d = d
        elif t is not None and D is not None:
            self.d = D - 2*t
        if D is not None:
            self.D = D
        elif t is not None and d is not None:
            self.D = d + 2*t
        assert self.d < self.D
        super().__init__(**kw)

    def _serialize(self):
        return "d = {d:.2f}mm, D = {D:.2f}mm, t = {t:.2f}mm, r = {r:.2f}mm".format(
                d = self.d / 1e-3,
                D = self.D / 1e-3,
                t = self.t / 1e-3,
                r = self.r / 1e-3)

    @property
    def Ktzd(self):
        """Formzahl bezüglich Zug/Druck"""
        return 1 + 1 / np.sqrt(.22*self.r/self.t + 2.74*self.r/self.d * (1 + 2*self.r/self.d)**2)
    @property
    def Ktb(self):
        """Formzahl bezüglich Biegung"""
        return 1 + 1 / np.sqrt(.2*self.r/self.t + 5.5*self.r/self.d * (1 + 2*self.r/self.d)**2)
    @property
    def Ktt(self):
        """Formzahl bezüglich Torsion"""
        return 1 + 1 / np.sqrt(3.4*self.r/self.t + 38*self.r/self.d * (1 + 2*self.r/self.d)**2 + 1 * (self.r/self.t)**3 * self.d/self.D)
    @property
    def Kts(self):
        """Formzahl bezüglich Schub"""
        return max(self.Ktzd, self.Ktb, self.Ktt)

    @property
    def Gsr(self):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Normalspannungen, F.55"""
        return 2 * (1 + self.phi) / self.r
    @property
    def Gtr(self):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Schubspannungen, F.55"""
        return 1 / self.r

class Spitzkerbe(KerbeKf):
    dP = 15e-3
    rP = 0.1e-3
    tP = 0.125 * 15e-3
    def __init__(self, r, **kw):
        d = kw.get('d')
        D = kw.get('D')
        t = kw.get('t')
        assert  r > 0, "`r` has to be positive"
        self.r = r
        assert sum(map(bool, (d, D, t))) >= 2, "please specify 2 of (d, D, t)"
        if d is not None:
            self.d = d
        elif t is not None and D is not None:
            self.d = D - 2*t
        if D is not None:
            self.D = D
        elif t is not None and d is not None:
            self.D = d + 2*t
        assert self.d < self.D
        super().__init__(**kw)

    @property
    def KfzdP(self):
        """Kerbwirkungszahl bewzüglich Zug/Druck, bezogen auf den Probendurchmesser dP"""
        return 1.06 + 1.1e-3 * self.Rm / 1e6
    @property
    def KfbP(self):
        """Kerbwirkungszahl bewzüglich Biegung, bezogen auf den Probendurchmesser dP"""
        return 0.97 + 0.95e-3 * self.Rm / 1e6
    @property
    def KftP(self):
        """Kerbwirkungszahl bewzüglich Torsion, bezogen auf den Probendurchmesser dP"""
        return 1 + 0.6 * (self.KfbP - 1)
    @property
    def KfsP(self):
        """Kerbwirkungszahl bewzüglich Schub, bezogen auf den Probendurchmesser dP"""
        return max(self.KfzdP, self.KfbP, self.KftP)

    def get_Gsr(self, **kw):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Normalspannungen, F.55"""
        return 2 * (1 + kw.get('phi', self.phi)) / kw.get('r', self.r)
    def get_Gtr(self, **kw):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Schubspannungen, F.55"""
        return 1 / kw.get('r', self.r)

class Passfedernut(KerbeKf):
    def __init__(self, r, **kw):
        d = kw.get('d')
        D = kw.get('D')
        t = kw.get('t')
        assert  r > 0, "`r` has to be positive"
        self.r = r
        assert sum(map(bool, (d, D, t))) >= 2, "please specify 2 of (d, D, t)"
        if d is not None:
            self.d = d
        elif t is not None and D is not None:
            self.d = D - 2*t
        if D is not None:
            self.D = D
        elif t is not None and d is not None:
            self.D = d + 2*t
        assert self.d < self.D
        super().__init__(**kw)

    @property
    def dP(self):
        """Probendurchmesser innen"""
        return self.d
    @property
    def tP(self):
        """Tiefe der Kerbe"""
        return self.t
    @property
    def rP(self):
        """Innenradius Kerbe"""
        return self.r

    @property
    def rho(self):
        """Rechengröße, siehe Formzahl-Anhang"""
        return .1e-3 if self.Rm <= 500e6 else .05e-3
    @property
    def KfzdP(self):
        """Kerbwirkungszahl bewzüglich Zug/Druck, bezogen auf den Probendurchmesser dP"""
        return 0.9 * (1.27 + 1.17 * np.sqrt(self.t / (self.r + 2.9 * self.rho)))
    @property
    def KfbP(self):
        """Kerbwirkungszahl bewzüglich Biegung, bezogen auf den Probendurchmesser dP"""
        return 0.9 * (1.14 + 1.08 * np.sqrt(self.t / (self.r + 2.9 * self.rho)))
    @property
    def KftP(self):
        """Kerbwirkungszahl bewzüglich Torsion, bezogen auf den Probendurchmesser dP"""
        return 1.48 + 0.45 * np.sqrt(self.t / (self.r + 2.9 * self.rho))
    @property
    def KfsP(self):
        """Kerbwirkungszahl bewzüglich Schub, bezogen auf den Probendurchmesser dP"""
        return max(self.KfzdP, self.KfbP, self.KftP)

    def get_Gsr(self, **kw):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Normalspannungen, F.55"""
        return 2 * (1 + kw.get('phi', self.phi)) / kw.get('r', self.r)
    def get_Gtr(self, **kw):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Schubspannungen, F.55"""
        return 1 / kw.get('r', self.r)

class Pressverband(KerbeKf):
    dP = 40e-3
    rP = .06 * dP
    nsrP = 1
    nsdP = 1
    ntrP = 1
    ntdP = 1
    _Kfb = {400: 1.8,
            500: 2.0,
            600: 2.1,
            700: 2.3,
            800: 2.5,
            900: 2.7,
            1000: 2.9,
            1100: 2.9,
            1200: 2.9}
    def __init__(self, d, Rm=None, **kw):
        self.d = d
        self.Rm = Rm
        super().__init__(**kw)

    @property
    def Rm(self):
        return self._Rm
    @Rm.setter
    def Rm(self, value):
        try:
            self._Rm = int(round(value / 1e6, -2))
        except TypeError:
            self._Rm = value

    @property
    def Kfb(self):
        return self._Kfb[self.Rm]
    @property
    def Kfzd(self):
        return self.Kfb
    @property
    def Kft(self):
        return 1 + .45 * (self.Kfb - 1)
    @property
    def Kfs(self):
        return max(self.Kfb, self.Kft)

    def get_Gsr(self, **kw):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Normalspannungen, F.55"""
        return 0
    def get_Gtr(self, **kw):
        """bezogener Spannungsgradient aufgrund des Kerbradius für Schubspannungen, F.55"""
        return 0
