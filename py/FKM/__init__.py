#!/usr/bin/env python3
# coding: utf-8

import numpy as np
from . import kerben

class NotYetImplementedError(RuntimeError):
    pass

class Werkstoff:
    def __init__(self, name, nummer, Rm, Re, S_Wzd, T_Ws, d_effNm, d_effNp, a_dm, a_dp, niro=False, randschicht_gehaertet=False):
        self.name = name
        self.nummer = nummer
        self.Rm = Rm
        self.Re = Re
        self.S_Wzd = S_Wzd
        self.T_Ws = T_Ws
        self.d_effNm = d_effNm
        self.d_effNp = d_effNp
        self.a_dm = a_dm
        self.a_dp = a_dp
        self.niro = niro
        self.rand_h = randschicht_gehaertet

    @property
    def a_G(self):
        """Hilfswert für plastische Stützzahlen, F.54"""
        return .4 if self.niro else .5
    @property
    def b_G(self):
        """Hilfswert für plastische Stützzahlen, F.54"""
        return 2400 if self.niro else 2700

class Belastungen:
    class Sub:
        def __init__(self):
            self._min = None
            self._max = None

        @staticmethod
        def _n(val):
            """Normalize value"""
            return val if val is not None else 0

        @property
        def min(self):
            return self._n(self._min)
        @min.setter
        def min(self, value):
            self._min = value
        @property
        def max(self):
            return self._n(self._max)
        @max.setter
        def max(self, value):
            self._max = value

        @property
        def med(self):
            return (self.max + self.min) / 2
        @property
        def amp(self):
            return abs((self.max - self.min)) / 2

        def __str__(self):
            return "max: {s.max:.3e}, min: {s.min:.3e}, med: {s.med:.3e}, amp: {s.amp:.3e}".format(s=self)
        def __bool__(self):
            return self.min != 0 or self.max != 0

    class _BaseTension:
        def __init__(self, parent, values):
            self.parent = parent
            self.values = values

        def calc(self, stress):
            """To be overridden"""
            return stress

        @property
        def min(self):
            return self.calc(self.values.min)
        @property
        def max(self):
            return self.calc(self.values.max)
        @property
        def med(self):
            return self.calc(self.values.med)
        @property
        def amp(self):
            return self.calc(self.values.amp)

        def __str__(self):
            return "max: {s.max:.3e}, min: {s.min:.3e}, med: {s.med:.3e}, amp: {s.amp:.3e}".format(s=self)

    class Tension_A(_BaseTension):
        def calc(self, stress):
            return stress / self.parent.A

    class Tension_Mb(_BaseTension):
        def calc(self, stress):
            return stress / self.parent.Wb

    class Tension_Mt(_BaseTension):
        def calc(self, stress):
            return stress / self.parent.Wp

    def __init__(self, parent):
        self.parent = parent

        self.N = self.Sub()
        self.Q = self.Sub()
        self.Mb = self.Sub()
        self.Mt = self.Sub()

        self.Szd = self.Tension_A(parent, self.N)
        self.Ts = self.Tension_A(parent, self.Q)
        self.Sb = self.Tension_Mb(parent, self.Mb)
        self.Tt = self.Tension_Mt(parent, self.Mt)

    def __str__(self):
        stress = "    N:   [N]   {s.N!s}\n" \
                 "    Q:   [N]   {s.Q!s}\n" \
                 "    Mb:  [Nm]  {s.Mb!s}\n"\
                 "    Mt:  [Nm]  {s.Mt!s}".format(s=self)
        tension = "    Szd: [Pa]  {s.Szd!s}\n" \
                  "    Ts:  [Pa]  {s.Ts!s}\n" \
                  "    Sb:  [Pa]  {s.Sb!s}\n" \
                  "    Tt:  [Pa]  {s.Tt!s}".format(s=self)
        return "Primäre Belastungen:\n{s}\n" \
               "Spannungen:\n{t}".format(s=stress, t=tension)

class WellenStueck:
    def __init__(self, d, material, kerbe=[], schadensfolge_hoch=True, p_spannung_hoch=True, regelm_inspektion=False, Rz=30, **kw):
        self._d = d
        self._d_eff = None

        if isinstance(material, dict):
            try:
                material = Werkstoff(**material)
            except TypeError:
                pass
        assert isinstance(material, Werkstoff), "Material ungültig"
        self.material = material

        self._belastungen = Belastungen(self)
        self.N = self._belastungen.N
        self.Q = self._belastungen.Q
        self.Mb = self._belastungen.Mb
        self.Mt = self._belastungen.Mt

        self._kerbe = None # initialize for getter
        self.kerbe = kerbe or kerben.Kerbe()  # empty container
        self.schadensfolge = schadensfolge_hoch
        self.p_spannung = p_spannung_hoch
        self.inspektion = regelm_inspektion
        self.Rz = Rz # Rauheit in µm

    @property
    def d(self):
        return self._d
    @d.setter
    def d(self, value):
        self._d = value
        if self.kerbe:
            for k in self.kerbe:
                k.d = value
    @property
    def d_eff(self):
        """effektiver Durchmesser für nicht kreisförmige Querschnitte"""
        return self._d_eff or self.d
    @d_eff.setter
    def d_eff(self, value):
        self._d_eff = value
    @property
    def kerbe(self):
        return self._kerbe
    @kerbe.setter
    def kerbe(self, kerbe):
        if not isinstance(kerbe, list):
            kerbe = [kerbe]
        for k in kerbe:
            if k.Rm is None:
                self._kerbe = True # needed by self.Rm
                k.Rm = self.Rm
        self._kerbe = kerbe

    @property
    def A(self):
        return np.pi * self.d**2 / 4
    @property
    def Wb(self):
        return np.pi * self.d**3 / 32
    @property
    def Wp(self):
        return np.pi * self.d**3 / 16

    @property
    def Kdm(self):
        """technologischer Größenfaktor für Zugfestigkeit, F.24"""
        if self.material.niro or self.d_eff <= self.material.d_effNm:
            return 1
        else:
            return (1 - 0.7686 * self.material.a_dm * np.log10(min(self.d_eff, 250e-3) / 7.5e-3)) / \
                   (1 - 0.7686 * self.material.a_dm * np.log10(self.material.d_effNm / 7.5e-3))
    @property
    def Kdp(self):
        """technologischer Größenfaktor für Fließgrenze, F.24"""
        if self.material.niro or self.d_eff <= self.material.d_effNm:
            return 1
        else:
            return (1 - 0.7686 * self.material.a_dp * np.log10(min(self.d_eff, 250e-3) / 7.5e-3)) / \
                   (1 - 0.7686 * self.material.a_dp * np.log10(self.material.d_effNp / 7.5e-3))
    @property
    def KA(self):
        """Anisotropiefaktor, F.25-30"""
        if self.kerbe or any((self.Q, self.Mt)):
            return 1
        else:
            if self.material.Rm <= 600e6:
                return 0.9
            elif self.material.Rm <= 900e6:
                return 0.86
            elif self.material.Rm <= 1200e6:
                return 0.83
            else:
                return 0.8

    @property
    def Rm(self):
        """Zugfestigkeit im Bauteil, F.30"""
        return self.material.Rm * self.Kdm * self.KA
    @property
    def Rp(self):
        """Fließgenze im Bauteil, F.30"""
        return self.material.Re * self.Kdp * self.KA

    @property
    def Kpb(self):
        """plastische Formzahl für Biegung, F.40"""
        return 1.7
    @property
    def Kpt(self):
        """plastische Formzahl für Torsion, F.40"""
        return 1.33
    @property
    def nplb(self):
        """plastische Stützzahl für Biegung, F.40-41"""
        if self.Rp <= 1050e6:
            return min(np.sqrt(1050e6/self.Rp), self.Kpb)
        else:
            return 1
    @property
    def nplt(self):
        """plastische Stützzahl für Torsion, F.40-41"""
        if self.Rp <= 1050e6:
            return min(np.sqrt(1050e6/self.Rp), self.Kpt)
        else:
            return 1

    @property
    def KSKzd(self):
        """Konstruktionsfaktor bezüglich Zug/Druck, F.42"""
        return 1
    @property
    def KSKb(self):
        """Konstruktionsfaktor bezüglich Biegung, F.42"""
        return 1 / self.nplb
    @property
    def KSKs(self):
        """Konstruktionsfaktor bezüglich Schub, F.42"""
        return 1
    @property
    def KSKt(self):
        """Konstruktionsfaktor bezüglich Torsion, F.42"""
        return 1 / self.nplt

    @property
    def SSKzd(self):
        """Bauteilfestigkeit bezüglich Zug/Druck, F.43"""
        return self.Rm / self.KSKzd
    @property
    def SSKb(self):
        """Bauteilfestigkeit bezüglich Biegung, F.43"""
        return self.Rm / self.KSKb
    @property
    def TSKs(self):
        """Bauteilfestigkeit bezüglich Schub, F.43"""
        return 0.577 * self.Rm / self.KSKs
    @property
    def TSKt(self):
        """Bauteilfestigkeit bezüglich Torsion, F.43"""
        return 0.577 * self.Rm / self.KSKt

    @property
    def jm(self):
        """Sicherheit gegen Bruch, F.44"""
        if not self.schadensfolge:
            if not self.p_spannung:
                return 1.6
            else:
                return 1.75
        else:
            if not self.p_spannung:
                return 1.8
            else:
                return 2.0
    @property
    def jp(self):
        """Sicherheit gegen Fließen, F.44"""
        if not self.schadensfolge:
            if not self.p_spannung:
                return 1.2
            else:
                return 1.3
        else:
            if not self.p_spannung:
                return 1.35
            else:
                return 1.5
    @property
    def jges(self):
        """Gesamtsicherheit, F.44"""
        return max(self.jm, self.jp * self.Rm / self.Rp)

    @property
    def aSKzd(self):
        """statische Auslastung Zug/Druck, F.45"""
        return max(np.abs(self._belastungen.Szd.max), np.abs(self._belastungen.Szd.min)) * self.jges / self.SSKzd
    @property
    def aSKb(self):
        """statische Auslastung Biegung, F.45"""
        return max(np.abs(self._belastungen.Sb.max), np.abs(self._belastungen.Sb.min)) * self.jges / self.SSKb
    @property
    def aSKs(self):
        """statische Auslastung Schub, F.45"""
        return max(np.abs(self._belastungen.Ts.max), np.abs(self._belastungen.Ts.min)) * self.jges / self.TSKs
    @property
    def aSKt(self):
        """statische Auslastung Torsion, F.45"""
        return max(np.abs(self._belastungen.Tt.max), np.abs(self._belastungen.Tt.min)) * self.jges / self.TSKt
    @property
    def aSK(self):
        """Gesamtauslastung statisch, F.46"""
        return np.sqrt((self.aSKzd + self.aSKb)**2 + (self.aSKs + self.aSKt)**2)

    # FKM dynamisch
    @property
    def sWzd(self):
        """Zugdruckwechselfestigkeit im Bauteil, F.49"""
        return self.Kdm * self.KA * self.material.S_Wzd
    @property
    def tWs(self):
        """Schubwechselfestigkeit im Bauteil, F.49"""
        return self.Kdm * self.KA * self.material.T_Ws

    @property
    def KRs(self):
        """Rauheitsfaktor bezüglich Normalspannungen, F.60"""
        return 1 - .22 * np.log10(self.Rz) * np.log10(2 * self.Rm / 400e6)
    @property
    def KRt(self):
        """Rauheitsfaktor bezüglich Schubspannungen, F.60"""
        return 1 - .577 * .22 * np.log10(self.Rz) * np.log10(2 * self.Rm / 400e6)

    @property
    def KV(self):
        """Randschichtverfestigungsfaktor, F.62"""
        return 1

    def _überlagerung(self, attr):
        K = 1
        for k in self.kerbe:
            if hasattr(k, attr):
                K += (getattr(k, attr) - 1)
        return K
    @property
    def Kfzd(self):
        return self._überlagerung('Kfzd')
    @property
    def Kfb(self):
        return self._überlagerung('Kfb')
    @property
    def Kfs(self):
        return self._überlagerung('Kfs')
    @property
    def Kft(self):
        return self._überlagerung('Kft')

    @property
    def Ktzd(self):
        return self._überlagerung('Ktzd')
    @property
    def Ktb(self):
        return self._überlagerung('Ktb')
    @property
    def Kts(self):
        return self._überlagerung('Kts')
    @property
    def Ktt(self):
        return self._überlagerung('Ktt')

    @property
    def KWKzd(self):
        """Konstruktionsfaktor Zug/Druck, F.64"""
        return (self.Kfzd + 1 / self.KRs - 1) / self.KV
    @property
    def KWKb(self):
        """Konstruktionsfaktor Biegung, F.64"""
        return (self.Kfb + 1 / self.KRs - 1) / self.KV
    @property
    def KWKs(self):
        """Konstruktionsfaktor Schub, F.64"""
        return (self.Kfs + 1 / self.KRt - 1) / self.KV
    @property
    def KWKt(self):
        """Konstruktionsfaktor Torsion, F.64"""
        return (self.Kft + 1 / self.KRt - 1) / self.KV

    @property
    def SWKzd(self):
        """ertragbare Bauteilwechselfestigkeit, bezogen auf Zug/Druck, F.65"""
        return self.sWzd / self.KWKzd
    @property
    def SWKb(self):
        """ertragbare Bauteilwechselfestigkeit, bezogen auf Biegung, F.65"""
        return self.sWzd / self.KWKb
    @property
    def TWKs(self):
        """ertragbare Bauteilwechselfestigkeit, bezogen auf Schub, F.65"""
        return self.tWs / self.KWKs
    @property
    def TWKt(self):
        """ertragbare Bauteilwechselfestigkeit, bezogen auf Torsion, F.65"""
        return self.tWs / self.KWKt

    @property
    def Msigma(self):
        """Mittelspannungsempfindlichkeit für Normalspannungen, F.66"""
        return .35e-3 * self.Rm / 1e6 - 0.1
    @property
    def Mtau(self):
        """Mittelspannungsempfindlichkeit für Schubspannungen, F.66"""
        return 0.577 * self.Msigma

    @property
    def Smv(self):
        """Vergleichsmittelnormalspannung, F.67"""
        return np.sqrt((self._belastungen.Szd.med + self._belastungen.Sb.med)**2 + \
                3 * (self._belastungen.Ts.med + self._belastungen.Tt.med)**2)
    @property
    def Tmv(self):
        """Vergleichsmittelschubspannung, F.67"""
        return 0.577 * self.Smv

    @property
    def SAKzd(self):
        """Ausschlagsspannung Zug/Druck, F.68 und Excel-Tabelle"""
        Sm = self.Smv
        k1 = 1 / (1 - self.Msigma)
        k2 = 1 - self.Msigma * Sm / self.SWKzd
        k3 = (1 + self.Msigma / 3) / (1 + self.Msigma) - self.Msigma / 3 * Sm / self.SWKzd
        k4 = (1 + self.Msigma / 3) / (1 + self.Msigma)**2
        SAK = self.SWKzd * (min(k1, k2) if Sm < 0 else max(k2, k3, k4))
        statische_grenze = self.SSKzd - abs(Sm)
        return max(min(statische_grenze, SAK), 0)
    @property
    def SAKb(self):
        """Ausschlagsspannung Biegung, F.68 und Excel-Tabelle"""
        Sm = self.Smv
        k1 = 1 / (1 - self.Msigma)
        k2 = 1 - self.Msigma * Sm / self.SWKb
        k3 = (1 + self.Msigma / 3) / (1 + self.Msigma) - self.Msigma / 3 * Sm / self.SWKb
        k4 = (1 + self.Msigma / 3) / (1 + self.Msigma)**2
        SAK = self.SWKb * (min(k1, k2) if Sm < 0 else max(k2, k3, k4))
        statische_grenze = self.SSKb - abs(Sm)
        return max(min(statische_grenze, SAK), 0)
    @property
    def TAKs(self):
        """Ausschlagsspannung Schub, F.68 und Excel-Tabelle"""
        Tm = self.Tmv
        k1 = 1 / (1 - self.Mtau)
        k2 = 1 - self.Mtau * Tm / self.TWKs
        k3 = (1 + self.Mtau / 3) / (1 + self.Mtau) - self.Mtau / 3 * Tm / self.TWKs
        k4 = (1 + self.Mtau / 3) / (1 + self.Mtau)**2
        TAK = self.TWKs * (min(k1, k2) if Tm < 0 else max(k2, k3, k4))
        statische_grenze = self.TSKs - abs(Tm)
        return max(min(statische_grenze, TAK), 0)
    @property
    def TAKt(self):
        """Ausschlagsspannung Torsion, F.68 und Excel-Tabelle"""
        Tm = self.Tmv
        k1 = 1 / (1 - self.Mtau)
        k2 = 1 - self.Mtau * Tm / self.TWKt
        k3 = (1 + self.Mtau / 3) / (1 + self.Mtau) - self.Mtau / 3 * Tm / self.TWKt
        k4 = (1 + self.Mtau / 3) / (1 + self.Mtau)**2
        TAK = self.TWKt * (min(k1, k2) if Tm < 0 else max(k2, k3, k4))
        statische_grenze = self.TSKt - abs(Tm)
        return max(min(statische_grenze, TAK), 0)

    @property
    def jD(self):
        """Sicherheitsfaktor für Dauerfestigkeitsnachweis"""
        if not self.schadensfolge:
            if self.inspektion:
                return 1.2
            else:
                return 1.3
        else:
            if self.inspektion:
                return 1.35
            else:
                return 1.5

    @property
    def aAKzd(self):
        """dynamische Auslastung Zug/Druck, F.72"""
        return self._belastungen.Szd.amp * self.jD / min(self.SAKzd, .75 * self.Rp)
    @property
    def aAKb(self):
        """dynamische Auslastung Biegung, F.72"""
        return self._belastungen.Sb.amp * self.jD / min(self.SAKb, .75 * self.Rp * self.Kpb)
    @property
    def aAKs(self):
        """dynamische Auslastung Schub, F.72"""
        return self._belastungen.Ts.amp * self.jD / min(self.TAKs, .75 * .577 * self.Rp)
    @property
    def aAKt(self):
        """dynamische Auslastung Torsion, F.72"""
        return self._belastungen.Tt.amp * self.jD / min(self.TAKt, .75 * .577 * self.Rp * self.Kpt)
    @property
    def aAK(self):
        """Gesamtauslastung dynamisch, F.72"""
        return np.sqrt((self.aAKzd + self.aAKb)**2 + (self.aAKs + self.aAKt)**2)

    def __str__(self):
        return \
            "Auslastungen statisch:\n" + \
            "  {:5s} = {:6.1%}\n".format("aSKzd", self.aSKzd) + \
            "  {:5s} = {:6.1%}\n".format("aSKb", self.aSKb) + \
            "  {:5s} = {:6.1%}\n".format("aSKs", self.aSKs) + \
            "  {:5s} = {:6.1%}\n".format("aSKt", self.aSKt) + \
            "  {:5s} = {:6.1%}\n".format("aSK", self.aSK) + \
            "Auslastungen dynamisch:\n" + \
            "  {:5s} = {:6.1%}\n".format("aAKzd", self.aAKzd) + \
            "  {:5s} = {:6.1%}\n".format("aAKb", self.aAKb) + \
            "  {:5s} = {:6.1%}\n".format("aAKs", self.aAKs) + \
            "  {:5s} = {:6.1%}\n".format("aAKt", self.aAKt) + \
            "  {:5s} = {:6.1%}".format("aAK", self.aAK)

