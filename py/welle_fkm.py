#!/usr/bin/env python3
# coding: utf-8

from FKM import WellenStueck
from FKM.kerben import Wellenabsatz, Umlaufkerbe
from FKM.werkstoffe import w_50CrMo4

# Wellenabsatz 'A' (Lager)
d_A = 120e-3  # Durchmesser der Welle (vor Absatz)
D_A = 140e-3  # Durchmesser der Welle (nach Absatz)
r_A = 2.5e-3  # Kerbradius
F_Lager = 760e3 / 2  # Lagerkraft
l_A = 55e-3 / 2  # Hebelarm Lagerkraft bis Absatz

wA = WellenStueck(d_A,
                  w_50CrMo4,
                  schadensfolge_hoch=False,
                  p_spannung_hoch=True,
                  regelm_inspektion=False)

wA.d_eff = 350e-3
wA.kerbe = Wellenabsatz(r=r_A, d=d_A, D=D_A)

wA.Mt.min = 0
wA.Mt.max = 13.33e3
wA.Q.min = -F_Lager
wA.Q.max = F_Lager
wA.Mb.min = -F_Lager * l_A
wA.Mb.max = F_Lager * l_A


wX = WellenStueck(d_A,
                  w_50CrMo4,
                  schadensfolge_hoch=False,
                  p_spannung_hoch=True,
                  regelm_inspektion=False)
wX.d_eff = 350e-3
wX.kerbe = Umlaufkerbe(r=.1e-3, D=d_A, t=5e-3)
wX.Mt.min = 0
wX.Mt.max = 13.33e3


# Wellenabsatz 'B' (Dichtbereich)
d_B = D_A
D_B = 350e-3
r_B = 5e-3
l_B = l_A + 35e-3

wB = WellenStueck(d_B,
                  w_50CrMo4,
                  schadensfolge_hoch=False,
                  p_spannung_hoch=True,
                  regelm_inspektion=False)

wB.d_eff = 350e-3
wB.kerbe = Wellenabsatz(r=r_B, d=d_B, D=D_B)

wB.Mt.min = wA.Mt.min
wB.Mt.max = wA.Mt.max
wB.Q.min = wA.Q.min
wB.Q.max = wA.Q.max
wB.Mb.min = -F_Lager * l_B
wB.Mb.max = F_Lager * l_B


if __name__ == '__main__':
    print("Lagersitz")
    print(wA, '\n')
    print("Nut für Sicherungsring")
    print(wX, '\n')
    print("Dichtbereich")
    print(wB)
