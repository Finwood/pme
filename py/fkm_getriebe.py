#!/usr/bin/env python3
# coding: utf-8

import numpy as np

from FKM import WellenStueck
from FKM.kerben import Wellenabsatz, Umlaufkerbe, Pressverband
from FKM.werkstoffe import w_50CrMo4

# Welle 2
from getriebe import w2, s1, s2


# Wellenabsatz 'A' (Lager)
d_A = 60e-3  # Durchmesser der Welle (vor Absatz)
D_A = d_A + 20e-3  # Durchmesser der Welle (nach Absatz)
r_A = 1e-3  # Kerbradius
F_Lager = w2.Fl3r  # Lagerkraft
B_Lager3 = 18e-3  # Lagerbreite
l_A = B_Lager3 / 2  # Hebelarm Lagerkraft bis Absatz

wA = WellenStueck(d_A,
                  w_50CrMo4,
                  schadensfolge_hoch=False,
                  p_spannung_hoch=True,
                  regelm_inspektion=False)

wA.d_eff = 110e-3
wA.kerbe = Wellenabsatz(r=r_A, d=d_A, D=D_A)

wA.N.min = 0
wA.N.max = w2.Fl3x
wA.Q.min = -F_Lager
wA.Q.max = F_Lager
wA.Mb.min = -F_Lager * l_A
wA.Mb.max = F_Lager * l_A


# Wellenabsatz 'B' (Anschlag Zahnrad)
d_B = D_A
D_B = d_B + 30e-3
r_B = 2e-3
l_B = w2.e3 + s1.b

wB = WellenStueck(d_B,
                  w_50CrMo4,
                  schadensfolge_hoch=False,
                  p_spannung_hoch=True,
                  regelm_inspektion=False)

wB.d_eff = wA.d_eff
wB.kerbe = [Wellenabsatz(r=r_B, d=d_B, D=D_B),
            Pressverband(d=d_B)]

wB.Mt.min = 0
wB.Mt.max = s1.M2
Q = np.hypot(w2.Qy(l_B), w2.Qz(l_B))
wB.Q.min = -Q
wB.Q.max = Q
M = np.hypot(w2.My(l_B), w2.Mz(l_B))
wB.Mb.min = -M
wB.Mb.max = M


# Wellenabsatz 'C' (Anschlag Ritzel)
D_C = D_B
d_C = D_C - 20e-3
r_C = 2e-3
l_C = w2.e5 + s2.b

wC = WellenStueck(d_C,
                  w_50CrMo4,
                  schadensfolge_hoch=False,
                  p_spannung_hoch=True,
                  regelm_inspektion=False)

wC.d_eff = wA.d_eff
wC.kerbe = [Wellenabsatz(r=r_C, d=d_C, D=D_C),
            Pressverband(d=d_C)]

wC.Mt.min = 0
wC.Mt.max = s2.M1
Q = np.hypot(w2.Qy(w2.L-l_C), w2.Qz(w2.L-l_C))
wC.Q.min = -Q
wC.Q.max = Q
M = np.hypot(w2.My(w2.L-l_C), w2.Mz(w2.L-l_C))
wC.Mb.min = -M
wC.Mb.max = M


# Wellenabsatz 'D' (Anschlag Lager Rechts)
D_D = d_C
d_D = D_D - 20e-3
r_D = 1e-3
B_Lager4 = 24e-3
l_D = B_Lager4 / 2 + 2e-3

wD = WellenStueck(d_D,
                  w_50CrMo4,
                  schadensfolge_hoch=False,
                  p_spannung_hoch=True,
                  regelm_inspektion=False)

wD.d_eff = wA.d_eff
wD.kerbe = Wellenabsatz(r=r_D, d=d_D, D=D_D)

wD.Mt.min = 0
wD.Mt.max = s2.M1
Q = np.hypot(w2.Qy(w2.L-l_D), w2.Qz(w2.L-l_D))
wD.Q.min = -Q
wD.Q.max = Q
M = np.hypot(w2.My(w2.L-l_D), w2.Mz(w2.L-l_D))
wD.Mb.min = -M
wD.Mb.max = M


if __name__ == '__main__':
    print("Lagersitz")
    print(d_A)
    print(wA, '\n')
    print("Anschlag Zahnrad")
    print(wB, '\n')
    print("Anschlag Ritzel")
    print(wC, '\n')
    print("Anschlag Lager rechts")
    print(d_D)
    print(wD, '\n')
