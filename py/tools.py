# coding: utf-8

import sys
from math import log, floor


def user(msg, dtype=str, allow_interrupt=False):
    while True:
        try:
            x = input(msg + ": ")
            try:
                val = dtype(x)
            except ValueError:
                print("{!r} expected, try again.\n".format(dtype))
                continue
            return (True, val) if allow_interrupt else val
        except (KeyboardInterrupt, EOFError):
            if allow_interrupt:
                return False, None
            raise

def hr(value, fmt='.0f', sep=' ', base=1000):
    mag = int(floor(log(abs(value), base)))
    if mag > 4:
        mag = 4
    if mag < -4:
        mag = -4

    # mag      0   1    2    3    4   -4   -3   -2   -1
    prefix = ['', 'k', 'M', 'G', 'T', 'p', 'n', 'µ', 'm']

    fmt_string = '{{0:{}}}{}{}'.format(fmt, sep, prefix[mag])
    return fmt_string.format(value / base**mag)


if sys.version_info >= (3, ):
    from tools3 import *
else:
    from tools2 import *
