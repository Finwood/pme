#!/usr/bin/env python3
# coding: utf-8

import numpy as np
from matplotlib import pyplot as plt

from tools import msg
from getriebe import w1, w2, w3, w4


def belastungen(w):
    x = np.linspace(0, w.L, 1000, endpoint=False)

    fig, (ax0, ax1, ax2a) = plt.subplots(3, 1, sharex=True, figsize=(6, 7))

    ax0.plot(x / 1e-3, w.N(x) / 1e3, 'k-', label="$N(x)$")
    ax0.plot(x / 1e-3, w.Qy(x) / 1e3, 'k--', label="$Q_y(x)$")
    ax0.plot(x / 1e-3, w.Qz(x) / 1e3, 'k-.', label="$Q_z(x)$")
    ax0.set_ylabel("Kraft in kN")
    ax0.legend(handlelength=4, loc=2)

    ax1.plot(x / 1e-3, w.Mt(x) / 1e3, 'k-', label="$M_t(x)$")
    ax1.plot(x / 1e-3, w.My(x) / 1e3, 'k--', label="$M_y(x)$")
    ax1.plot(x / 1e-3, w.Mz(x) / 1e3, 'k-.', label="$M_z(x)$")
    ax1.set_ylabel("Moment in kNm")
    ax1.legend(handlelength=4, loc=4)

    hs = ax2a.plot(x / 1e-3, np.hypot(w.Qy(x), w.Qz(x)) / 1e3, 'k-',
                   label=r"$\sqrt{Q_y^2 + Q_z^2}$")
    ax2a.set_ylabel("Querkraft in kN")
    ax2b = ax2a.twinx()
    hs += ax2b.plot(x / 1e-3, np.hypot(w.My(x), w.Mz(x)) / 1e3, 'k--',
                   label=r"$\sqrt{M_y^2 + M_z^2}$")
    ax2b.set_ylabel("Biegemoment in kNm")
    ax2b.grid(False)
    ax2b.legend(handles=hs, handlelength=4, loc=2)

    ax2a.set_xlabel("x in mm")
    ax2a.set_xlim(0, w.L / 1e-3)

    for ax in ax0, ax1, ax2a:
        for start, end in w.zahnräder:
            ax.axvspan(start/1e-3, end/1e-3, facecolor='k', alpha=0.15)

    fig.tight_layout()
    return fig


# Tabellenbuch Metall S.44, 50CrMo4
sigma_b_wechselnd = 480e6
tau_t_schwellend = 560e6

sigma_zul = sigma_b_wechselnd
tau_zul = tau_t_schwellend
nu = 4

def vorauslegung(w):
    x = np.linspace(0, w.L, 1000, endpoint=False)
    Mt = np.abs(w.Mt(x))
    Mb = np.hypot(w.My(x), w.Mz(x))

    d_t = (16 * Mt * nu / tau_zul / np.pi)**(1/3)
    d_b = (32 * Mb * nu / sigma_zul / np.pi)**(1/3)

    fig, ax = plt.subplots(1, 1, figsize=(6, 3))

    ax.plot(x / 1e-3, d_t / 1e-3, 'k--', label=r"$d_{va,t}$")
    ax.plot(x / 1e-3, d_b / 1e-3, 'k-', label=r"$d_{va,b}$")
    ax.set_ylabel("Wellendurchmesser in mm")
    ax.set_xlabel("x in mm")
    ax.set_xlim(0, w.L / 1e-3)
    ax.legend(handlelength=4)

    for start, end in w.zahnräder:
        ax.axvspan(start/1e-3, end/1e-3, facecolor='k', alpha=0.15)

    fig.tight_layout()
    return fig


if __name__ == '__main__':
    for w, name in [(w1, "welle1"), (w2, "welle2"),
                    (w3, "welle3"), (w4, "welle4")]:
        with msg("Belastungen {}".format(name)):
            fig = belastungen(w)
            fig.savefig('../tex/img/belastungen-' + name + '.png')
        with msg("Vorauslegung {}".format(name)):
            fig = vorauslegung(w)
            fig.savefig('../tex/img/vorauslegung-' + name + '.png')
