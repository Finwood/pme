# coding: utf-8

from contextlib import contextmanager
import time

from tools import hr

@contextmanager
def msg(m, end="done.", timing=True):
    print m + '...',
    t_start = time.time()
    yield
    if timing:
        t = time.time() - t_start
        end = "{s:{d}s}[{hrt:<7s}s]".format(s=end, d=80-len(m)-15,
                                            hrt=hr(t, fmt='5.1f'))
    print end
